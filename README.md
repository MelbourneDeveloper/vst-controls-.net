# VST Controls .Net #

![VST Controls .Net.png](https://bitbucket.org/repo/rEjXbd/images/3826375051-VST%20Controls%20.Net.png)

A collection of controls written in C# designed for synthesis user interfaces on the Microsoft platforms: .Net, and Universal Windows (UWP).

The controls are designed to be image skinnable, and lightweight enough to be used for VST instrument user interfaces, or other multimedia purposes. All code is written in C#. 

Current controls:

* Fader

* Knob

* Switch


*Note: The software, images and code base are only free to use and modify for non-commercial use. These controls are in the beta phase, so bug reports, feedback, and code submissions are welcomed. Mail: mail at syncretia.com* 

[End User License Agreement (EULA)](https://bitbucket.org/MelbourneDeveloper/vst-controls-.net/wiki/VST%20Controls%20.Net%20EULA)