﻿#region Using
using System;
using System.Threading.Tasks;
using Windows.Storage;
#endregion Using

namespace VSTControls.Net
{
    internal static partial class UtilUri
    {
        #region Private Static Fields
        private static readonly string basePath = String.Empty;
        #endregion Private Static Fields

        #region Internal Static Methods
        internal static async Task<bool> IsUriExists(String path)
        {
            try
            {
                // Best way to test this is to catch the exception.
                return await StorageFile.GetFileFromApplicationUriAsync(new Uri(path)) != null;
            }
            catch
            {
                return false;
            }
        }
        #endregion Internal Static Methods
    }
}
