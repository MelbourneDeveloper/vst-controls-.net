﻿#region Using
using System;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;
#endregion Using

namespace VSTControls.Net
{
    internal static partial class UtilBitmap
    {
        #region Private Static Fields
        private static readonly String basePath = String.Empty;
        #endregion Private Static Fields

        #region Internal Static Methods
        internal static async Task<BitmapUWP> LoadSpriteSheetAsync(String firstSpritePath, int spriteSize)
        {
            byte[] srcPixels;
            byte[] spriteSheetBuffer = null;
            WriteableBitmap spriteSheet = null;
            WriteableBitmap sprite;
            string filename;
            string directory;
            int spriteCount = 0;
            int spriteIndex = 0;

            // Extract path components, convention is "directory" + "filename" + "image suffix number mask" + "extension"
            UtilUri.ExtractUriComponents(firstSpritePath, out directory, out filename);

            // Check image suffix
            if (filename.Contains(UtilUri.SpriteSuffix))
            {
                // Get sprite count
                spriteCount = await GetSpriteCount(filename, directory);

                // Load first sprite
                BitmapUWP bitmapUWP = await UtilBitmap.LoadBitmap(firstSpritePath);
                sprite = bitmapUWP.source;
                srcPixels = bitmapUWP.buffer;
                
                // Load sprite sheet
                spriteSheet = new WriteableBitmap(spriteSize, spriteCount * spriteSize);
                spriteSheetBuffer = new byte[srcPixels.Length * spriteCount];

                // Set-up sprite sheet write stream
                Stream pixelStream = spriteSheet.PixelBuffer.AsStream();
                pixelStream.Seek(0, SeekOrigin.Begin);

                // Copy all sprites to sprite sheet
                while (spriteIndex < spriteCount &&
                       sprite.PixelWidth.Equals(spriteSize) &&
                       sprite.PixelHeight.Equals(spriteSize))
                {
                    // Copy sprite pixel buffer to sprite sheet pixels buffer
                    srcPixels.CopyTo(spriteSheetBuffer, spriteIndex * srcPixels.Length);

                    // Append sprite pixel buffer into sprite sheet
                    pixelStream.Write(srcPixels, 0, srcPixels.Length);

                    // Load next sprite
                    if (++spriteIndex < spriteCount)
                    {
                        BitmapUWP uwpBitmap2 = await LoadBitmap(UtilUri.BuildSpriteUri(spriteIndex,
                                                                                           directory,
                                                                                           filename));
                        sprite = uwpBitmap2.source;
                        srcPixels = uwpBitmap2.buffer;
                    }
                }
            }

            return spriteCount > 0 && spriteIndex.Equals(spriteCount) ? new BitmapUWP(spriteSheet, spriteSheetBuffer) : BitmapUWP.Null;
        }

        internal static async Task<BitmapUWP> LoadBitmap(String uri)
        { 
            // Bitmap information
            WriteableBitmap bitmap = null;
            byte[] srcPixels = null;

            if (String.IsNullOrWhiteSpace(uri))
            {
                throw new Exception(Message.Text[Error.LoadImage]);
            }

            // From web server
            //using (IRandomAccessStreamWithContentType fileStream = await RandomAccessStreamReference.CreateFromUri(new Uri(uri)).OpenReadAsync())
            // From resource
            using (IRandomAccessStream fileStream = await (await StorageFile.GetFileFromApplicationUriAsync(new Uri(uri))).OpenAsync(FileAccessMode.Read))
            {
                BitmapFrame frame = await (await BitmapDecoder.CreateAsync(fileStream)).GetFrameAsync(0);
                srcPixels = (await frame.GetPixelDataAsync()).DetachPixelData();
                bitmap = new WriteableBitmap((int)frame.PixelWidth, (int)frame.PixelHeight);
            }

            if (bitmap == null)
            {
                throw new Exception(Message.Text[Error.LoadImage]);
            }

            return new BitmapUWP(bitmap, srcPixels);
        }
        #endregion Internal Static Methods

        #region Private Static Methods
        private static async Task<int> GetSpriteCount(string filename, string directory)
        {
            int spriteCount = 0;

            while (await UtilUri.IsUriExists(UtilUri.BuildSpriteUri(spriteCount, directory, filename)))
            {
                spriteCount++;
            }

            return spriteCount;
        }
        #endregion Private Static Methods
    }
}