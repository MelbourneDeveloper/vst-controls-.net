﻿#region Using
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Markup;
#endregion Using

namespace VSTControls.Net
{
    [TemplatePart(Name = "PART_VIEWBOX", Type = typeof(Viewbox))]
    [TemplatePart(Name = "PART_CANVAS", Type = typeof(Canvas))]
    [TemplatePart(Name = "PART_BACKGROUND_IMAGE", Type = typeof(Image))]
    [TemplatePart(Name = "PART_FOREGROUND_IMAGE", Type = typeof(Image))]
    [TemplatePart(Name = "PART_THUMB_IMAGE", Type = typeof(Image))]
    public sealed partial class Slider
    {
        #region Public Constructor
        public Slider()
        {
            Template = CreateTemplate();
            ControlBase.SetRenderMode(this, bitmap.Lock);
        }
        #endregion Public Constructor
        
        #region Protected Overrides
        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            InvalidateMeasure();
            InvalidateVisual();
        }
        #endregion Protected Overrides

        #region Private Static Methods
        private static ControlTemplate CreateTemplate()
        {
            // Haven't found substitute for the ControlTemplate code-behind factory in WinRT
            const string xaml = @"<ControlTemplate xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation"" 
                                                   xmlns:x=""http://schemas.microsoft.com/winfx/2006/xaml"">
                                      <Viewbox x:Name=""PART_VIEWBOX"" Stretch=""Uniform"">
                                          <Canvas x:Name=""PART_CANVAS"" Background=""Transparent"">
                                              <Image x:Name=""PART_BACKGROUND_IMAGE""/>
                                              <Image x:Name=""PART_FOREGROUND_IMAGE""/>
                                              <Image x:Name=""PART_THUMB_IMAGE""/>
                                          </Canvas>
                                      </Viewbox>
                                  </ControlTemplate>";

            return (ControlTemplate)XamlReader.Load(xaml);
        }
        #endregion Private Static Methods

        #region Private Methods
        private void InvalidateVisual()
        {
            OnRender((Viewbox)GetTemplateChild("PART_VIEWBOX"),
                     (Canvas)GetTemplateChild("PART_CANVAS"),
                     (Image)GetTemplateChild("PART_BACKGROUND_IMAGE"),
                     (Image)GetTemplateChild("PART_FOREGROUND_IMAGE"),
                     (Image)GetTemplateChild("PART_THUMB_IMAGE"));
        }
        #endregion Private Methods
    }
}