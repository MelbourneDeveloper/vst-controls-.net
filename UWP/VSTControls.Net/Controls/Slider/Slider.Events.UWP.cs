﻿#region Using
using System;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
#endregion Using

namespace VSTControls.Net
{
    public sealed partial class Slider
    {
        #region Protected Overrides
        protected override Size MeasureOverride(Size availableSize)
        {
            bitmap.controlSize = availableSize;
            InvalidateVisual();

            return availableSize;
        }
        protected override void OnKeyDown(KeyRoutedEventArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (IsEnabled &&
                IsKeyboardEnabled &&
                KeyEventArgs.VirtualKeyToKey.ContainsKey(args.Key))
            {
                OnKeyDown(KeyEventArgs.VirtualKeyToKey[args.Key]);
                Focus(FocusState.Keyboard);
                args.Handled = true;
            }
            else
            {
                base.OnKeyDown(args);
            }
        }

        protected sealed override void OnPointerCaptureLost(PointerRoutedEventArgs args)
        {
            if (args == null ||
                args.Pointer == null)
            {
                return;
            }

            if (IsEnabled &&
                args.Pointer.IsInContact)
            {
                Focus(FocusState.Keyboard);
                args.Handled = true;
            }
            else
            {
                base.OnPointerCaptureLost(args);
            }
        }

        protected sealed override void OnPointerMoved(PointerRoutedEventArgs args)
        {
            if (args == null ||
                args.Pointer == null)
            {
                return;
            }

            if (IsEnabled &&
                args.Pointer.IsInContact)
            {
                OnMouseDown(args.GetCurrentPoint((FrameworkElement)GetTemplateChild("PART_CANVAS")).RawPosition);
                Focus(FocusState.Keyboard);
                args.Handled = true;
            }
            else
            {
                base.OnPointerMoved(args);
            }
        }

        protected sealed override void OnPointerPressed(PointerRoutedEventArgs args)
        {
            if (args == null ||
                args.Pointer == null)
            {
                return;
            }

            if (IsEnabled &&
                args.Pointer.IsInContact)
            {
                OnMouseDown(args.GetCurrentPoint((FrameworkElement)GetTemplateChild("PART_CANVAS")).RawPosition);
                CapturePointer(args.Pointer);
                Focus(FocusState.Keyboard);
                args.Handled = true;
            }
            else
            {
                base.OnPointerPressed(args);
            }
        }

        protected sealed override void OnPointerWheelChanged(PointerRoutedEventArgs args)
        {
            if (args == null ||
                args.Pointer == null)
            {
                return;
            }

            if (IsEnabled)
            {
                PointerPoint pointerPoint = args.GetCurrentPoint(this);

                if (pointerPoint != null &&
                    pointerPoint.Properties != null)
                {
                    int delta = pointerPoint.Properties.MouseWheelDelta;

                    if (!delta.Equals(0))
                    {
                        OnMouseWheel(delta);
                        Focus(FocusState.Keyboard);
                        args.Handled = true;
                    }
                }
            }
            else
            {
                base.OnPointerWheelChanged(args);
            }
        }
        #endregion Protected Overrides

        #region Private Static Methods
        private static void OnPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            Slider @this = (Slider)sender;

            if (@this == null)
            {
                return;
            }

            lock (@this.bitmap.Lock)
            {
                @this.InvalidateMeasure();
                @this.InvalidateVisual();
            }
        }
        #endregion Private Static Methods
        
        #region Private Methods
        private async void OnRender(Viewbox viewbox, Canvas canvas, Image backgroundImage, Image foregroundImage, Image thumbImage)
        {
            const double half = 0.5d;
            const double rightAngle = 90d;
            double minimum;
            double maximum;
            double value;

            try
            {
                lock (bitmap.Lock)
                {
                    // Properties
                    minimum = Minimum;
                    maximum = Maximum;
                    value = UtilMath.ClampValue(Value, minimum, maximum);
                }

                // Load background
                bitmap.image = null;
                try { BitmapUWP.AssignBitmap(await UtilBitmap.LoadBitmap(BackgroundImage), ref bitmap); } catch { }

                lock (bitmap.Lock)
                {
                    // Background part
                    if (bitmap.image != null)
                    {
                        // Set control size
                        bitmap.SetRectangle(new Size(bitmap.image.PixelWidth, bitmap.image.PixelHeight));

                        // Set image source to display bitmap
                        backgroundImage.Source = WriteToBitmap(bitmap);
                    }
                    else
                    {
                        bitmap.SetRectangle(bitmap.controlSize);
                    }
                }

                // Load foreground
                bitmap.image = null;
                try { BitmapUWP.AssignBitmap(await UtilBitmap.LoadBitmap(ForegroundImage), ref bitmap); } catch { }

                lock (bitmap.Lock)
                {
                    // Foreground part
                    if (bitmap.image != null)
                    {
                        // Set control size
                        bitmap.SetRectangle(new Size(bitmap.image.PixelWidth, bitmap.image.PixelHeight));

                        // Set image source to display bitmap
                        foregroundImage.Source = WriteToBitmap(bitmap);
                        
                        // Compute foreground Y position in control
                        int y = (int)UtilMath.ConvertRange(value,
                                                           minimum,
                                                           maximum,
                                                           bitmap.rectangle.Height,
                                                           0d);

                        // Clip foreground image
                        foregroundImage.Clip = new RectangleGeometry()
                        {
                            Rect = new Rect(0, y, bitmap.rectangle.Width, bitmap.rectangle.Height - y)
                        };
                    }
                }

                // Load thumb image
                bitmap.image = null;
                try { BitmapUWP.AssignBitmap(await UtilBitmap.LoadBitmap(ThumbImage), ref bitmap); } catch { }

                lock (bitmap.Lock)
                {
                    // Thumb part
                    if (bitmap.image != null)
                    {
                        // Get thumb size
                        Size thumbSize = new Size(bitmap.image.PixelWidth, bitmap.image.PixelHeight);

                        // Compute thumb Y position in control
                        int y = (int)UtilMath.ConvertRange(value,
                                                           minimum,
                                                           maximum,
                                                           bitmap.rectangle.Height - thumbSize.Height,
                                                           0d);

                        // Set image source to display bitmap
                        thumbImage.Source = WriteToBitmap(bitmap);
                        
                        // Set thumb Y position
                        Canvas.SetTop(thumbImage, y);
                    }

                    // Set canvas size
                    canvas.Width = bitmap.rectangle.Width;
                    canvas.Height = bitmap.rectangle.Height;
                    
                    // Set image stretch
                    viewbox.Stretch = ImageStretch ? Stretch.Uniform : Stretch.None;

                    // Set orientation
                    bool isVertical = Orientation.Equals(Orientation.Vertical);
                    RenderTransformOrigin = isVertical ? new Point() : new Point(half, half);
                    
                    RenderTransform = isVertical ? null : new RotateTransform()
                    {
                        Angle = rightAngle
                    };
                }
            }
            catch (Exception exception)
            {
                Message.Output(Message.Text[Error.LoadImage], exception.ToString());
            }
        }

        private WriteableBitmap WriteToBitmap(Bitmap sourceBitmap)
        {
            WriteableBitmap destination = new WriteableBitmap(sourceBitmap.image.PixelWidth, sourceBitmap.image.PixelHeight);

            // Copy sprite buffer from sprite sheet in display bitmap
            Stream pixelStream = destination.PixelBuffer.AsStream();
            pixelStream.Seek(0, SeekOrigin.Begin);
            pixelStream.Write(sourceBitmap.buffer, 0, sourceBitmap.buffer.Length);

            return destination;
        }        
        #endregion Private Methods
    }
}