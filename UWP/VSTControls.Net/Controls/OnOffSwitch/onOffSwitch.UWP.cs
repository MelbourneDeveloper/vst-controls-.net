﻿#region Using
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Markup;
#endregion Using

namespace VSTControls.Net
{
    [TemplatePart(Name = "PART_IMAGE", Type = typeof(Image))]
    public sealed partial class OnOffSwitch
    {
        #region Public Constructor
        public OnOffSwitch()
        {
            Template = CreateTemplate();
            ControlBase.SetRenderMode(this, bitmap.Lock);
        }
        #endregion Public Constructor
        
        #region Protected Overrides
        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            InvalidateMeasure();
            InvalidateVisual();
        }
        #endregion Protected Overrides

        #region Private Static Methods
        private static ControlTemplate CreateTemplate()
        {
            // Haven't found substitute for the ControlTemplate code-behind factory in WinRT
            const string xaml = @"<ControlTemplate xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation"" 
                                                   xmlns:x=""http://schemas.microsoft.com/winfx/2006/xaml"">
                                      <Image x:Name=""PART_IMAGE""/>
                                  </ControlTemplate>";

            return (ControlTemplate)XamlReader.Load(xaml);
        }
        #endregion Private Static Methods

        #region Private Methods
        private void InvalidateVisual()
        {
            OnRender((Image)GetTemplateChild("PART_IMAGE"));
        }
        #endregion Private Methods
    }
}