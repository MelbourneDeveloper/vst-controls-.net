﻿#region Using
using System;
using Windows.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media.Imaging;
#endregion Using

namespace VSTControls.Net
{
    public partial class OnOffSwitch
    {
        #region Protected Overrides
        protected sealed override void OnKeyDown(KeyRoutedEventArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (IsEnabled &&
                IsKeyboardEnabled &&
                KeyEventArgs.VirtualKeyToKey.ContainsKey(args.Key))
            {
                Focus(FocusState.Keyboard);
                args.Handled = true;
            }
            else
            {
                base.OnKeyDown(args);
            }
        }

        protected sealed override void OnKeyUp(KeyRoutedEventArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (IsEnabled &&
                IsKeyboardEnabled &&
                args.KeyStatus.RepeatCount.Equals(0) &&
                KeyEventArgs.VirtualKeyToKey.ContainsKey(args.Key))
            {
                OnKeyUp(KeyEventArgs.VirtualKeyToKey[args.Key]);
                Focus(FocusState.Keyboard);
                args.Handled = true;
            }
            else
            {
                base.OnKeyUp(args);
            }
        }

        protected sealed override void OnPointerCaptureLost(PointerRoutedEventArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (IsEnabled &&
                args.Pointer.IsInContact)
            {
                OnMouseUp(args.GetCurrentPoint(this).RawPosition);
                Focus(FocusState.Keyboard);
                args.Handled = true;
            }
            else
            {
                base.OnPointerCaptureLost(args);
            }
        }

        protected sealed override void OnPointerPressed(PointerRoutedEventArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (IsEnabled &&
                args.Pointer.IsInContact)
            {
                CapturePointer(args.Pointer);
                Focus(FocusState.Keyboard);
                args.Handled = true;
            }
            else
            {
                base.OnPointerPressed(args);
            }
        }

        protected sealed override void OnPointerWheelChanged(PointerRoutedEventArgs args)
        {
            if (args == null ||
                args.Pointer == null)
            {
                return;
            }

            if (IsEnabled &&
                args.Pointer.IsInContact)
            {
                PointerPoint pointerPoint = args.GetCurrentPoint(this);

                if (pointerPoint != null &&
                    pointerPoint.Properties != null)
                {
                    int delta = pointerPoint.Properties.MouseWheelDelta;

                    if (!delta.Equals(0))
                    {
                        OnMouseWheel(delta);
                        Focus(FocusState.Keyboard);
                        args.Handled = true;
                    }
                }
            }
            else
            {
                base.OnPointerWheelChanged(args);
            }
        }
        #endregion Protected Overrides

        #region Private Static Methods
        private static void OnPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            OnOffSwitch @this = (OnOffSwitch)sender;

            if (@this == null)
            {
                return;
            }

            lock (@this.bitmap.Lock)
            {
                @this.InvalidateMeasure();
                @this.InvalidateVisual();
            }
        }
        #endregion Private Static Methods

        #region Private Methods
        private async void OnRender(Image image)
        {
            State state;
            string imagePath;

            // Load image
            try
            {
                lock (bitmap.Lock)
                {
                    // Get state based on IsOn property value
                    ObjectToState.TryGetValue(IsOn, out state);

                    // Get image path based on state value
                    StateToImagePath.TryGetValue(state, out imagePath);
                }

                bitmap.image = (await UtilBitmap.LoadBitmap(imagePath)).source;

                lock (bitmap.Lock)
                {
                    // Set image source to display bitmap
                    image.Source = bitmap.image;
                }
            }
            catch (Exception exception)
            {
                Message.Output(Message.Text[Error.LoadImage], exception.ToString());
            }
        }        
        #endregion Private Methods
    }
}