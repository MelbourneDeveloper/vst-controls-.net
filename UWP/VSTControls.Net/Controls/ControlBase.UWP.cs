﻿#region Using
using Windows.UI.Xaml;
#endregion Using

namespace VSTControls.Net
{
    internal static class ControlBase
    {
        public static void SetRenderMode(FrameworkElement @this, object @lock)
        {
            @this.UseLayoutRounding = true;
        }
    }
}
