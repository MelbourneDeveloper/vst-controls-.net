﻿#region Using
using System;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
#endregion Using

namespace VSTControls.Net
{
    public sealed partial class Knob
    {
        #region Protected Overrides
        protected override void OnKeyDown(KeyRoutedEventArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (IsEnabled &&
                IsKeyboardEnabled &&
                KeyEventArgs.VirtualKeyToKey.ContainsKey(args.Key))
            {
                OnKeyDown(KeyEventArgs.VirtualKeyToKey[args.Key]);
                Focus(FocusState.Keyboard);
                args.Handled = true;
            }
            else
            {
                base.OnKeyDown(args);
            }
        }

        protected sealed override void OnPointerCaptureLost(PointerRoutedEventArgs args)
        {
            if (args == null ||
                args.Pointer == null)
            {
                return;
            }

            if (IsEnabled &&
                args.Pointer.IsInContact)
            {
                Focus(FocusState.Keyboard);
                args.Handled = true;
            }
            else
            {
                base.OnPointerCaptureLost(args);
            }
        }

        protected override void OnPointerMoved(PointerRoutedEventArgs args)
        {
            if (args == null ||
                args.Pointer == null)
            {
                return;
            }

            if (IsEnabled && 
                args.Pointer.IsInContact)
            {
                OnMouseMove(args.GetCurrentPoint(this).RawPosition);
                Focus(FocusState.Keyboard);
                args.Handled = true;
            }
            else
            {
                base.OnPointerMoved(args);
            }
        }
        
        protected override void OnPointerPressed(PointerRoutedEventArgs args)
        {
            if (args == null ||
                args.Pointer == null)
            {
                return;
            }

            if (IsEnabled && 
                args.Pointer.IsInContact)
            {
                OnMouseDown(args.GetCurrentPoint(this).RawPosition);
                CapturePointer(args.Pointer);
                Focus(FocusState.Keyboard);
                args.Handled = true;
            }
            else
            {
                base.OnPointerPressed(args);
            }
        }

        protected sealed override void OnPointerWheelChanged(PointerRoutedEventArgs args)
        {
            if (args == null ||
                args.Pointer == null)
            {
                return;
            }

            if (IsEnabled)
            {
                PointerPoint pointerPoint = args.GetCurrentPoint(this);

                if (pointerPoint != null &&
                    pointerPoint.Properties != null)
                {
                    int delta = pointerPoint.Properties.MouseWheelDelta;

                    if (!delta.Equals(0))
                    {
                        OnMouseWheel(delta);
                        Focus(FocusState.Keyboard);
                        args.Handled = true;
                    }
                }
            }
            
            if (!args.Handled)
            {
                base.OnPointerWheelChanged(args);
            }
        }
        #endregion Protected Overrides

        #region Private Static Methods
        private static void OnPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            Knob @this = (Knob)sender;

            if (@this == null)
            {
                return;
            }

            lock (@this.bitmap.Lock)
            {
                @this.InvalidateVisual();
            }
        }
        #endregion Private Static Methods

        #region Private Methods
        private double ConvertAngleToValue(Point mousePosition)
        {
            return UtilMath.ConvertAngleToValue(new Rect(0d, 0d, ActualWidth, ActualHeight),
                                                mousePosition,
                                                StartAngle,
                                                EndAngle,
                                                Minimum,
                                                Maximum);
        }

        private async void OnRender(Image image)
        {
            try
            {
                // Properties
                double minimum = Minimum;
                double maximum = Maximum;
                double value = UtilMath.ClampValue(Value, minimum, maximum);

                // Load sprite
                if (bitmap.image == null)
                {
                    BitmapUWP.AssignBitmap(await UtilBitmap.LoadBitmap(KnobImage), ref bitmap);
                }

                if (bitmap.image == null)
                {
                    return;
                }
                
                // Load sprite sheet
                if (bitmap.image.PixelWidth.Equals(bitmap.image.PixelHeight))
                {
                    BitmapUWP.AssignBitmap(await UtilBitmap.LoadSpriteSheetAsync(KnobImage, bitmap.image.PixelWidth), ref bitmap);
                }

                lock (bitmap.Lock)
                {
                    if (image == null ||
                        bitmap.image == null)
                    {
                        return;
                    }

                    // Compute image index
                    int imageIndex = (int)UtilMath.ConvertRange(value,
                                                                minimum,
                                                                maximum,
                                                                0d,
                                                                bitmap.image.PixelHeight / bitmap.image.PixelWidth - 1d);

                    // Compute sprite Y position in spritesheet
                    int y = UtilBitmap.ComputeSpritePositionInSpriteSheet(imageIndex,
                                                                          bitmap.image.PixelWidth,
                                                                          bitmap.image.PixelHeight);

                    // Compute bounds
                    bitmap.SetRectangle(new Size(bitmap.image.PixelWidth, bitmap.image.PixelWidth));

                    // Create display bitmap
                    WriteableBitmap destination = new WriteableBitmap(bitmap.image.PixelWidth, bitmap.image.PixelWidth);

                    // Compute sprite Y position in sprite sheet
                    int spritePixelCount = bitmap.buffer.Length / (bitmap.image.PixelHeight / bitmap.image.PixelWidth);
                    int yPositionInSpritesheet = imageIndex * spritePixelCount;

                    // Copy sprite buffer from sprite sheet in display bitmap
                    Stream pixelStream = destination.PixelBuffer.AsStream();
                    pixelStream.Seek(0, SeekOrigin.Begin);
                    pixelStream.Write(bitmap.buffer, yPositionInSpritesheet, spritePixelCount);

                    // Set image source to display bitmap
                    image.Source = destination;

                    // Set image stretch behavior
                    image.Stretch = ImageStretch ? Stretch.Fill : Stretch.None;
                }
            }
            catch (Exception exception)
            {
                Message.Output(Message.Text[Error.LoadImage], exception.ToString());
            }
        }
        #endregion Private Methods
    }
}