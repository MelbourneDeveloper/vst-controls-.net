﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace VSTControls.Net
{
    public sealed partial class Picture : Control
    {
        #region Private Static Methods
        private static void OnPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            Picture @this = (Picture)sender;

            if (@this == null)
            {
                return;
            }

            lock (@this.bitmap.Lock)
            {
                @this.InvalidateMeasure();
                @this.InvalidateVisual();
            }
        }
        #endregion Private Static Methods

        #region Private Methods
        private async void OnRender(Image image)
        {
            try
            {
                bitmap.image = (await UtilBitmap.LoadBitmap(PictureImage)).source;

                lock (bitmap.Lock)
                {
                    // Set image source to display bitmap
                    image.Source = bitmap.image;
                }
            }
            catch (Exception exception)
            {
                Message.Output(Message.Text[Error.LoadImage], exception.ToString());
            }
        }
        #endregion Private Methods
    }
}