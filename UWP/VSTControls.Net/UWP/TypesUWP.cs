﻿#region Using
using System;
using System.Collections.Generic;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Imaging;
#endregion Using

namespace VSTControls.Net
{
    /// <summary>
    /// Provides mock types for WPF within UWP
    /// </summary>
    
    #region Internal Enums
    internal enum FrameworkPropertyMetadataOptions
    {
        BindsTwoWayByDefault = 256
    }

    internal enum Key
    {
        Enter = 6,
        Space = 18,
        PageUp = 19,
        PageDown = 20,
        End = 21,
        Home = 22,
        Left = 23,
        Up = 24,
        Right = 25,
        Down = 26
    }
    #endregion Internal Enums
    
    #region Internal Struct
    internal struct Int32Rect
    {
    }
    #endregion Internal Struct

    #region Internal Class
    internal class KeyEventArgs
    {
        internal static Dictionary<VirtualKey, Key> VirtualKeyToKey = new Dictionary<VirtualKey, Key>(Enum.GetValues(typeof(Key)).Length)
        {
            { VirtualKey.Enter, Key.Enter },
            { VirtualKey.Space, Key.Space },
            { VirtualKey.PageUp, Key.PageUp },
            { VirtualKey.PageDown, Key.PageDown },
            { VirtualKey.End, Key.End },
            { VirtualKey.Home, Key.Home },
            { VirtualKey.Left, Key.Left },
            { VirtualKey.Up, Key.Up },
            { VirtualKey.Right, Key.Right },
            { VirtualKey.Down, Key.Down }
        };
    }
    
    internal class CroppedBitmap : BitmapSource
    {
        internal CroppedBitmap(BitmapSource source, Int32Rect sourceRect)
        {
        }
    }
    
    internal class FrameworkPropertyMetadata : PropertyMetadata
    {
        internal FrameworkPropertyMetadata(object defaultValue, FrameworkPropertyMetadataOptions flags, PropertyChangedCallback propertyChangedCallback) : base(defaultValue, propertyChangedCallback)
        {
        }
    }
    #endregion Internal Class
}