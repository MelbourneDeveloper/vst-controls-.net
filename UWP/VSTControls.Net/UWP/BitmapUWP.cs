﻿#region Using
using Windows.UI.Xaml.Media.Imaging;
#endregion Using

namespace VSTControls.Net
{
    /// <summary>
    /// Container for fetching more than 1 parameter out of async functions
    /// </summary>
    internal struct BitmapUWP
    {
        #region Internal Static Properties
        internal static BitmapUWP Null
        {
            get
            {
                return new BitmapUWP();
            }
        }
        #endregion Internal Static Properties

        #region Internal Fields
        internal WriteableBitmap source;
        internal byte[] buffer;
        #endregion Internal Fields

        #region Internal Constructor
        internal BitmapUWP(WriteableBitmap source, byte[] buffer)
        {
            this.source = source;
            this.buffer = buffer;
        }
        #endregion Internal Constructor

        #region Internal Static Methods
        internal static void AssignBitmap(BitmapUWP bitmapUWP, ref Bitmap bitmap)
        {
            lock (bitmap.Lock)
            {
                bitmap.buffer = bitmapUWP.buffer;
                bitmap.image = bitmapUWP.source;
            }
        }
        #endregion Internal Static Methods
    }
}