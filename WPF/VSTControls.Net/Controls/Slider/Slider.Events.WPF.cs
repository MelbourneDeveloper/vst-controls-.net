﻿#region Using
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.ComponentModel;
#endregion Using

namespace VSTControls.Net
{
    public sealed partial class Slider
    {
        #region Protected Overrides
        protected sealed override Size MeasureOverride(Size constraint)
        {
            lock (bitmap.Lock)
            {
                try
                {
                    // Control size
                    Size controlSize = constraint;

                    // Load foreground
                    WriteableBitmap foreground = UtilBitmap.LoadBitmap(ForegroundImage);

                    if (foreground != null)
                    {
                        controlSize = new Size(foreground.PixelWidth, foreground.PixelHeight);
                    }

                    // Load background
                    WriteableBitmap background = UtilBitmap.LoadBitmap(BackgroundImage);

                    if (background != null)
                    {
                        controlSize = new Size(background.PixelWidth, background.PixelHeight);
                    }

                    // Aspect ratio
                    controlSize = ImageStretch ? new Size(constraint.Height * (controlSize.Width / controlSize.Height), constraint.Height) : controlSize;

                    // Set control size
                    bitmap.SetRectangle(controlSize);

                    return controlSize;
                }
                catch (Exception exception)
                {
                    Message.Output(Message.Text[Error.LoadImage], exception.ToString());
                    return base.MeasureOverride(constraint);
                }
            }
        }

        protected sealed override void OnPreviewKeyDown(KeyEventArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (IsEnabled &&
                IsKeyboardEnabled &&
                IsKeyboardFocused)
            {
                OnKeyDown(args.Key);
                SetEventHandled(args);
            }
            else
            {
                base.OnPreviewKeyDown(args);
            }
        }

        protected sealed override void OnPreviewKeyUp(KeyEventArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (IsEnabled && 
                IsKeyboardEnabled &&
                IsKeyboardFocused)
            {
                SetEventHandled(args);
            }
            else
            {
                base.OnPreviewKeyUp(args);
            }
        }

        protected sealed override void OnPreviewMouseDown(MouseButtonEventArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (IsEnabled &&
                !IsMouseCaptured &&
                IsMouseOver &&
                (args.ChangedButton.Equals(MouseButton.Left) ||
                 args.ChangedButton.Equals(MouseButton.Middle)))
            {
                OnMouseDown(args.GetPosition(this));
                CaptureMouse();
                SetEventHandled(args);
            }
            else
            {
                base.OnPreviewMouseDown(args);
            }
        }

        protected sealed override void OnPreviewMouseMove(MouseEventArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (IsEnabled && 
                IsMouseCaptured)
            {
                OnMouseMove(args.GetPosition(this));
                args.Handled = true;
            }
            else
            {
                base.OnPreviewMouseMove(args);
            }
        }

        protected sealed override void OnPreviewMouseUp(MouseButtonEventArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (IsMouseCaptured)
            {
                ReleaseMouseCapture();

                if (IsEnabled)
                {
                    SetEventHandled(args);
                }
            }
            else
            {
                base.OnPreviewMouseUp(args);
            }
        }

        protected sealed override void OnPreviewMouseWheel(MouseWheelEventArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (IsEnabled &&
                IsMouseOver &&
                !args.Delta.Equals(0))
            {
                OnMouseWheel(args.Delta);
                SetEventHandled(args);
            }
            else
            {
                base.OnPreviewMouseWheel(args);
            }
        }

        protected sealed override void OnPropertyChanged(DependencyPropertyChangedEventArgs args)
        {
            lock (bitmap.Lock)
            {
                if (args.Property.Equals(WidthProperty))
                {
                    Width = double.NaN;
                }
                else if (args.Property.Equals(HeightProperty))
                {
                    Height = double.NaN;
                }
            }

            base.OnPropertyChanged(args);
        }

        protected sealed override void OnRender(DrawingContext drawingContext)
        {
            const double bestDpi = 96d;

            if (drawingContext == null)
            {
                return;
            }

            lock (bitmap.Lock)
            {
                try
                {
                    // Control size
                    Size controlSize = new Size(bitmap.rectangle.Width, bitmap.rectangle.Height);
                    Size realSize = new Size();

                    // Properties
                    bool isImageStretch = ImageStretch;
                    bool isVertical = Orientation.Equals(Orientation.Vertical);
                    double minimum = Minimum;
                    double maximum = Maximum;
                    double value = UtilMath.ClampValue(Value, minimum, maximum);

                    // Load thumb image
                    WriteableBitmap thumb = UtilBitmap.LoadBitmap(ThumbImage);

                    if (thumb != null)
                        realSize = new Size(thumb.PixelWidth, 10d * thumb.PixelHeight);

                    // Load foreground
                    WriteableBitmap foreground = UtilBitmap.LoadBitmap(ForegroundImage);

                    if (foreground != null)
                        realSize = new Size(foreground.PixelWidth, foreground.PixelHeight);

                    // Load background
                    WriteableBitmap background = UtilBitmap.LoadBitmap(BackgroundImage);

                    if (background != null)
                        realSize = new Size(background.PixelWidth, background.PixelHeight);
                    
                    // Use invisible background to register mouse click
                    if (background == null)
                    {
                        background = new WriteableBitmap((int)controlSize.Width,
                                                         (int)controlSize.Height,
                                                         bestDpi,
                                                         bestDpi,
                                                         PixelFormats.Bgra32,
                                                         null);
                    }
                    
                    // Draw background
                    if (background != null)
                    {
                        drawingContext.DrawImage(background,
                                                 new Rect(0d,
                                                          0d,
                                                          controlSize.Width,
                                                          controlSize.Height));
                    }

                    // Draw foreground
                    if (foreground != null)
                    {
                        // Compute foreground Y position in control
                        int realY = (int)UtilMath.ConvertRange(value,
                                                               minimum,
                                                               maximum,
                                                               foreground.PixelHeight,
                                                               0d);

                        double controlY = UtilMath.ConvertRange(value,
                                                                minimum,
                                                                maximum,
                                                                controlSize.Height,
                                                                0d);

                        if (realY >= 0 && 
                            realY < foreground.PixelHeight &&
                            foreground.PixelHeight - realY > 0 &&
                            controlY >= 0 &&
                            controlY < controlSize.Height &&
                            controlSize.Height - controlY > 0)
                        {
                            drawingContext.DrawImage(new CroppedBitmap(foreground,
                                                                       new Int32Rect(0,
                                                                                     realY,
                                                                                     foreground.PixelWidth,
                                                                                     (int)foreground.PixelHeight - realY)),
                                                     new Rect(0d,
                                                              controlY,
                                                              controlSize.Width,
                                                              controlSize.Height - controlY));
                        }
                    }

                    // Thumb part
                    if (thumb != null)
                    {
                        // Get thumb size
                        Size realThumbSize = new Size(thumb.PixelWidth, thumb.PixelHeight);
                        Size controlThumbSize = new Size(controlSize.Width, isImageStretch ? controlSize.Width * (realThumbSize.Height / realThumbSize.Width) : realThumbSize.Height);

                        // Compute thumb Y position in control
                        int realY = (int)UtilMath.ConvertRange(value,
                                                               minimum,
                                                               maximum,
                                                               realSize.Height - realThumbSize.Height,
                                                               0d);

                        double controlY = UtilMath.ConvertRange(value,
                                                                minimum,
                                                                maximum,
                                                                controlSize.Height - controlThumbSize.Height,
                                                                0d);

                        // Draw thumb
                        drawingContext.DrawImage(thumb, 
                                                 new Rect(0d, 
                                                          controlY, 
                                                          controlThumbSize.Width,
                                                          controlThumbSize.Height));
                    }
                }
                catch (Exception exception)
                {
                    Message.Output(Message.Text[Error.LoadImage], exception.ToString());
                    base.OnRender(drawingContext);
                }
            }
        }
        #endregion Protected Overrides
        
        #region Private Static Methods
        private static void OnPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            if (sender == null ||
                args == null ||
                args.Property == null)
            {
                return;
            }

            SliderProp sliderProp;

            if (Enum.TryParse(args.Property.Name, out sliderProp))
            {
                // Dispatch property change to message pump for further processing
                MessagePump<SliderProp>(sender, args.NewValue, sliderProp);
            }
        }
        #endregion Private Static Methods

        #region Private Methods
        private void SetEventHandled(RoutedEventArgs args)
        {
            if (IsKeyboardEnabled)
            {
                FocusManager.SetIsFocusScope(this, true);
                KeyboardNavigation.SetDirectionalNavigation(this, KeyboardNavigationMode.None);
                Keyboard.Focus(this);
            }

            args.Handled = true;
        }
        #endregion Private Methods
    }
}