﻿#region Using
using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
#endregion Using

namespace VSTControls.Net
{
    public sealed partial class Knob
    {
        #region Protected Overrides
        protected sealed override Size MeasureOverride(Size constraint)
        {
            lock (bitmap.Lock)
            {
                try
                {
                    // Control size
                    Size controlSize = constraint;

                    // Load knob image
                    WriteableBitmap knobImage = UtilBitmap.LoadBitmap(KnobImage);

                    if (knobImage != null)
                    {
                        controlSize = new Size(knobImage.PixelWidth, knobImage.PixelWidth);
                    }

                    // Aspect ratio
                    double shorterSide = constraint.Width < constraint.Height ? constraint.Width : constraint.Height;
                    controlSize = ImageStretch ? new Size(shorterSide, shorterSide) : controlSize;

                    // Set control size
                    bitmap.SetRectangle(controlSize);
                    
                    return controlSize;
                }
                catch (Exception exception)
                {
                    Message.Output(Message.Text[Error.LoadImage], exception.ToString());
                    return base.MeasureOverride(constraint);
                }
            }
        }

        protected sealed override void OnPreviewKeyDown(KeyEventArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (IsEnabled &&
                IsKeyboardEnabled &&
                IsKeyboardFocused)
            {
                OnKeyDown(args.Key);
                SetEventHandled(args);
            }
            else
            {
                base.OnPreviewKeyDown(args);
            }
        }

        protected sealed override void OnPreviewKeyUp(KeyEventArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (IsEnabled &&
                IsKeyboardEnabled &&
                IsKeyboardFocused)
            {
                SetEventHandled(args);
            }
            else
            {
                base.OnPreviewKeyUp(args);
            }
        }

        protected sealed override void OnPreviewMouseDown(MouseButtonEventArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (IsEnabled &&
                !IsMouseCaptured &&
                IsMouseOver &&
                (args.ChangedButton.Equals(MouseButton.Left) ||
                 args.ChangedButton.Equals(MouseButton.Middle)))
            {
                OnMouseDown(args.GetPosition(this));
                CaptureMouse();
                SetEventHandled(args);
            }
            else
            {
                base.OnPreviewMouseDown(args);
            }
        }

        protected sealed override void OnPreviewMouseMove(MouseEventArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (IsEnabled &&
                IsMouseCaptured)
            {
                OnMouseMove(args.GetPosition(this));
                args.Handled = true;
            }
            else
            {
                base.OnPreviewMouseMove(args);
            }
        }

        protected sealed override void OnPreviewMouseUp(MouseButtonEventArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (IsEnabled &&
                IsMouseCaptured)
            {
                ReleaseMouseCapture();
                SetEventHandled(args);
            }
            else
            {
                base.OnPreviewMouseUp(args);
            }
        }

        protected sealed override void OnPreviewMouseWheel(MouseWheelEventArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (IsEnabled &&
                IsMouseOver &&
                !args.Delta.Equals(0))
            {
                OnMouseWheel(args.Delta);
                SetEventHandled(args);
            }
            else
            {
                base.OnPreviewMouseWheel(args);
            }
        }

        protected sealed override void OnRender(DrawingContext drawingContext)
        {
            lock (bitmap.Lock)
            {
                try
                {
                    // Properties
                    double minimum = Minimum;
                    double maximum = Maximum;
                    double value = UtilMath.ClampValue(Value, minimum, maximum);

                    // Load sprite
                    WriteableBitmap knobImage = UtilBitmap.LoadBitmap(KnobImage);

                    // Get sprite size
                    int spriteSize = knobImage.PixelWidth;
                    int spriteSheetHeight = knobImage.PixelHeight;

                    // Load sprite sheet
                    if (spriteSize.Equals(spriteSheetHeight))
                    {
                        knobImage = (WriteableBitmap)UtilBitmap.LoadSpriteSheet(KnobImage, spriteSize);
                    }

                    // Compute image index
                    int imageIndex = (int)UtilMath.ConvertRange(value,
                                                                minimum,
                                                                maximum,
                                                                0d,
                                                                spriteSheetHeight / spriteSize - 1d);

                    // Compute sprite Y position in spritesheet
                    int y = UtilBitmap.ComputeSpritePositionInSpriteSheet(imageIndex,
                                                                          spriteSize, 
                                                                          spriteSheetHeight);
                    
                    // Draw to screen
                    drawingContext.DrawImage(new CroppedBitmap(knobImage, new Int32Rect(0,
                                                                                        y,
                                                                                        spriteSize,
                                                                                        spriteSize)),
                                             bitmap.rectangle);
                }
                catch (Exception exception)
                {
                    Message.Output(Message.Text[Error.LoadImage], exception.ToString());
                    base.OnRender(drawingContext);
                }
            }
        }
        #endregion Protected Overrides
        
        #region Private Static Methods
        private static void OnPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            if (sender == null ||
                args == null ||
                args.Property == null)
            {
                return;
            }

            KnobProp knobProp;

            if (Enum.TryParse(args.Property.Name, out knobProp))
            {
                // Dispatch property change to message pump for further processing
                MessagePump<KnobProp>(sender, args.NewValue, knobProp);
            }
        }
        #endregion Private Static Methods

        #region Private Methods
        private void SetEventHandled(RoutedEventArgs args)
        {
            FocusManager.SetIsFocusScope(this, true);
            KeyboardNavigation.SetDirectionalNavigation(this, KeyboardNavigationMode.None);
            Keyboard.Focus(this);
            args.Handled = true;
        }

        private double ConvertAngleToValue(Point mousePosition)
        {
            return UtilMath.ConvertAngleToValue(bitmap.rectangle,
                                                mousePosition,
                                                StartAngle,
                                                EndAngle,
                                                Minimum,
                                                Maximum);
        }
        #endregion Private Methods
    }
}