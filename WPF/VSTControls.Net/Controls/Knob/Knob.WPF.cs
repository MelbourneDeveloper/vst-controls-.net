﻿using System;

namespace VSTControls.Net
{
    [CLSCompliant(true)]
    public sealed partial class Knob
    {
        #region Public Constructor
        /// <summary>
        /// Class for VSTControls.Net Knob control
        /// </summary>
        public Knob()
        {
            ControlBase.SetRenderMode(this, bitmap.Lock);
        }
        #endregion Public Constructor
    }
}