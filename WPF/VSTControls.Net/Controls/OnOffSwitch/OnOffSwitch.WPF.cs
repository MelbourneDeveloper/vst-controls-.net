﻿#region Using
using System;
#endregion Using

namespace VSTControls.Net
{
    [CLSCompliant(true)]
    public partial class OnOffSwitch
    {
        #region Public Constructor
        /// <summary>
        /// Class for VSTControls.Net OnOffSwitch control
        /// </summary>
        public OnOffSwitch()
        {
            ControlBase.SetRenderMode(this, bitmap.Lock);

            lock (bitmap.Lock)
            {
                foreach (State stateKey in Enum.GetValues(typeof(State)))
                {
                    StateToImagePath.Add(stateKey, null);
                }
            }
        }
        #endregion Public Constructor
    }
}