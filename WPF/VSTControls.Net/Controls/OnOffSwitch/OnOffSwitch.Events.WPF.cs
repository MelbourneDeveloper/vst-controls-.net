﻿#region Using
using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
#endregion Using

namespace VSTControls.Net
{
    public partial class OnOffSwitch
    {
        #region Protected Overrides
        protected sealed override Size MeasureOverride(Size constraint)
        {
            // Local variables
            State state;
            string imagePath;

            // Control size
            Size controlSize = constraint;

            // Load switch image for current state
            if (ObjectToState.TryGetValue(IsOn, out state) &&
                StateToImagePath.TryGetValue(State.Off, out imagePath))
            {
                WriteableBitmap switchImage = UtilBitmap.LoadBitmap(imagePath);

                if (switchImage != null)
                {
                    controlSize = new Size(switchImage.PixelWidth, switchImage.PixelHeight);
                }
            }

            // Aspect ratio
            double shorterSide = constraint.Width < constraint.Height ? constraint.Width : constraint.Height;
            controlSize = ImageStretch ? new Size(shorterSide, shorterSide) : controlSize;

            // Set control size
            bitmap.SetRectangle(controlSize);

            return controlSize;
        }

        protected sealed override void OnPreviewKeyDown(KeyEventArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (IsEnabled &&
                IsKeyboardEnabled &&
                IsKeyboardFocused)
            {
                SetEventHandled(args);
            }
            else
            {
                base.OnPreviewKeyDown(args);
            }
        }

        protected sealed override void OnPreviewKeyUp(KeyEventArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (IsEnabled &&
                IsKeyboardEnabled &&
                IsKeyboardFocused)
            {
                if (!args.IsRepeat)
                {
                    OnKeyUp(args.Key);
                }

                SetEventHandled(args);
            }
            else
            {
                base.OnPreviewKeyUp(args);
            }
        }

        protected sealed override void OnPreviewMouseDown(MouseButtonEventArgs args)
        {
            if (args == null)
            {
                return;
            }
            
            if (IsEnabled &&
                !IsMouseCaptured &&
                IsMouseOver &&
                (args.ChangedButton.Equals(MouseButton.Left) ||
                 args.ChangedButton.Equals(MouseButton.Middle)))
            {
                CaptureMouse();
                SetEventHandled(args);
            }
            else
            {
                base.OnPreviewMouseDown(args);
            }
        }

        protected sealed override void OnPreviewMouseUp(MouseButtonEventArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (IsMouseCaptured)
            {
                ReleaseMouseCapture();

                if (IsEnabled &&
                    IsMouseOver && 
                    (args.ChangedButton.Equals(MouseButton.Left) ||
                     args.ChangedButton.Equals(MouseButton.Middle)))
                {
                    OnMouseUp(args.GetPosition(this));
                    SetEventHandled(args);
                }
            }
            else
            {
                base.OnPreviewMouseUp(args);
            }
        }
        
        protected sealed override void OnPreviewMouseWheel(MouseWheelEventArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (IsEnabled &&
                IsMouseOver &&
                !args.Delta.Equals(0))
            {
                OnMouseWheel(args.Delta);
                SetEventHandled(args);
            }
            else
            {
                base.OnPreviewMouseWheel(args);
            }
        }

        protected sealed override void OnRender(DrawingContext drawingContext)
        {
            State state;
            string imagePath;

            lock (bitmap.Lock)
            {
                try
                {
                    // Get state based on IsOn property value
                    ObjectToState.TryGetValue(IsOn, out state);

                    // Get image path based on state value
                    StateToImagePath.TryGetValue(state, out imagePath);
                    
                    // Load image
                    WriteableBitmap switchImage = UtilBitmap.LoadBitmap(imagePath);

                    // Draw to screen
                    drawingContext.DrawImage(switchImage, bitmap.rectangle);
                }
                catch (Exception exception)
                {
                    Message.Output(Message.Text[Error.LoadImage], exception.ToString());
                    base.OnRender(drawingContext);
                }
            }
        }
        #endregion Protected Overrides

        #region Private Static Methods
        private static void OnPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            if (sender == null ||
                args == null ||
                args.Property == null)
            {
                return;
            }

            OnOffSwitchProp onOffSwitchProp;

            if (Enum.TryParse(args.Property.Name, out onOffSwitchProp))
            {
                // Dispatch property change to message pump for further processing
                MessagePump<OnOffSwitchProp>(sender, args.NewValue, onOffSwitchProp);
            }
        }
        #endregion Private Static Methods
        
        #region Private Methods
        private void SetEventHandled(RoutedEventArgs args)
        {
            FocusManager.SetIsFocusScope(this, true);
            KeyboardNavigation.SetDirectionalNavigation(this, KeyboardNavigationMode.None);
            Keyboard.Focus(this);
            args.Handled = true;
        }
        #endregion Private Methods
    }
}