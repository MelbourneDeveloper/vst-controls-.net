﻿#region Using
using System;
using System.Windows;
using System.Windows.Media;
#endregion Using

[assembly: CLSCompliant(true)]
namespace VSTControls.Net
{
    internal static class ControlBase
    {
        public static void SetRenderMode(FrameworkElement @this, object @lock)
        {
            lock (@lock)
            {
                @this.BeginInit();
                @this.FocusVisualStyle = null;
                @this.SnapsToDevicePixels = true;
                @this.UseLayoutRounding = true;
                RenderOptions.SetBitmapScalingMode(@this, BitmapScalingMode.HighQuality);
                RenderOptions.SetEdgeMode(@this, EdgeMode.Aliased);
                @this.EndInit();
            }
        }
    }
}
