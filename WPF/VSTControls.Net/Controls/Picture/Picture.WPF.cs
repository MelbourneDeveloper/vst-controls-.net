﻿#region Using
using System;
#endregion Using

namespace VSTControls.Net
{
    [CLSCompliant(true)]
    public sealed partial class Picture
    {
        #region Public Constructor
        /// <summary>
        /// Class for VSTControls.Net Picture control
        /// </summary>
        public Picture()
        {
            ControlBase.SetRenderMode(this, bitmap.Lock);
        }
        #endregion Public Constructor
    }
}