﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace VSTControls.Net
{
    public sealed partial class Picture : Control
    {
        #region Protected Overrides
        protected sealed override Size MeasureOverride(Size constraint)
        {
            lock (bitmap.Lock)
            {
                try
                {
                    // Control size
                    Size controlSize = constraint;

                    if (!ImageStretch)
                    {
                        // Load image
                        WriteableBitmap image = UtilBitmap.LoadBitmap(PictureImage);

                        if (image != null)
                        {
                            controlSize = new Size(image.PixelWidth, image.PixelHeight);
                        }
                    }

                    // Set control size
                    bitmap.SetRectangle(controlSize);

                    return controlSize;
                }
                catch (Exception exception)
                {
                    Message.Output(Message.Text[Error.LoadImage], exception.ToString());
                    return base.MeasureOverride(constraint);
                }
            }
        }

        protected sealed override void OnRender(DrawingContext drawingContext)
        {
            lock (bitmap.Lock)
            {
                try
                {
                    // Load image
                    WriteableBitmap image = UtilBitmap.LoadBitmap(PictureImage);

                    // Draw to screen
                    drawingContext.DrawImage(image, bitmap.rectangle);
                }
                catch (Exception exception)
                {
                    Message.Output(Message.Text[Error.LoadImage], exception.ToString());
                    base.OnRender(drawingContext);
                }
            }
        }
        #endregion Protected Overrides

        #region Private Static Methods
        private static void OnPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            if (sender == null ||
                args == null ||
                args.Property == null)
            {
                return;
            }

            PictureProp pictureProp;

            if (Enum.TryParse(args.Property.Name, out pictureProp))
            {
                // Dispatch property change to message pump for further processing
                MessagePump<KnobProp>(sender, args.NewValue, pictureProp);
            }
        }
        #endregion Private Static Methods
    }
}