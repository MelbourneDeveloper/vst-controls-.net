﻿#region Using
using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
#endregion Using

namespace VSTControls.Net
{
    internal static partial class UtilBitmap
    {
        #region Internal Static Methods
        internal static WriteableBitmap LoadSpriteSheet(string firstSpritePath, int spriteSize)
        {
            Int32Rect spriteRectangle = new Int32Rect(0, 0, spriteSize, spriteSize);
            WriteableBitmap sprite;
            WriteableBitmap spriteSheet = null;
            string filename;
            string directory;
            int spriteCount = 0;
            int spriteIndex = 0;

            // Extract path components, convention is "directory" + "filename" + "image suffix number mask" + "extension"
            UtilUri.ExtractUriComponents(firstSpritePath, out directory, out filename);

            // Check image suffix
            if (filename.Contains(UtilUri.SpriteSuffix))
            {
                // Get sprite count
                spriteCount = GetSpriteCount(filename, directory);

                // Load first sprite
                sprite = LoadBitmap(firstSpritePath);

                // Load sprite sheet
                spriteSheet = new WriteableBitmap(spriteSize,
                                                  spriteCount * spriteSize,
                                                  sprite.DpiX,
                                                  sprite.DpiY,
                                                  sprite.Format,
                                                  sprite.Palette);

                // Copy all sprites to sprite sheet
                while (spriteIndex < spriteCount && 
                       sprite.PixelWidth.Equals(spriteSize) && 
                       sprite.PixelHeight.Equals(spriteSize))
                {
                    // Copy sprite to sprite sheet
                    CopySpriteToSpriteSheet(spriteIndex,
                                            spriteSize,
                                            spriteRectangle,
                                            sprite,
                                            spriteSheet);
                    
                    // Load next sprite
                    if (++spriteIndex < spriteCount)
                    {
                        sprite = LoadBitmap(UtilUri.BuildSpriteUri(spriteIndex, directory, filename));
                    }
                }
            }

            return spriteCount > 0 && spriteIndex.Equals(spriteCount) ? spriteSheet : null;
        }

        internal static WriteableBitmap LoadBitmap(string path)
        {
            /// http://www.hanselman.com/blog/BeAwareOfDPIWithImagePNGsInWPFImagesScaleWeirdOrAreBlurry.aspx
            const double bestDpi = 96d;
            
            // Invalid path
            if (string.IsNullOrWhiteSpace(path))
                return null;

            // Lost from cache
            WriteableBitmap bitmap = Cache.GetBitmap(path);

            if (bitmap != null)
                return bitmap;

            // Use BitmapImage to load bitmap from URI
            BitmapImage bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            RenderOptions.SetBitmapScalingMode(bitmapImage, BitmapScalingMode.HighQuality);
            RenderOptions.SetEdgeMode(bitmapImage, EdgeMode.Aliased);
            bitmapImage.UriSource = new Uri(path, UriKind.RelativeOrAbsolute);
            bitmapImage.EndInit();
            bitmapImage.Freeze();

            // Save the BitmapSource of BitmapImage in the BitmapSource of WriteableBitmap
            bitmap = new WriteableBitmap(bitmapImage);
            Cache.AddBitmap(path, bitmap);

            // Check DPI
            if (!bitmapImage.DpiX.Equals(bestDpi) ||
                !bitmapImage.DpiY.Equals(bestDpi))
            {
                Message.Output(Message.Text[Warning.WrongDpi], path);
            }

            return bitmap;
        }
        #endregion Internal Static Methods

        #region Private Static Methods
        private static void CopySpriteToSpriteSheet(int spriteIndex, int spriteSize, Int32Rect spriteRectangle, WriteableBitmap sprite, WriteableBitmap spriteSheet)
        {
            // Compute pixel metadata
            int stride = spriteSize * sprite.Format.BitsPerPixel;
            byte[] buffer = new byte[stride * spriteSize];

            // Copy sprite pixel buffer   
            sprite.CopyPixels(buffer, stride, 0);

            // Paste sprite pixel buffer into sprite sheet
            spriteSheet.WritePixels(spriteRectangle,
                                    buffer,
                                    stride,
                                    0,
                                    spriteIndex * spriteSize);
        }

        private static int GetSpriteCount(string filename, string directory)
        {
            int spriteCount = 0;

            while (UtilUri.IsUriExists(UtilUri.BuildSpriteUri(spriteCount, directory, filename)))
            {
                spriteCount++;
            }

            return spriteCount;
        }
        #endregion Private Static Methods
    }
}