﻿#region Using
using System;
using System.IO;
using System.Reflection;
using System.Windows;
#endregion Using

namespace VSTControls.Net
{
    internal static partial class UtilUri
    {
        #region Private Static Fields
        private static readonly string basePath = Path.GetDirectoryName(Uri.UnescapeDataString(new UriBuilder(Assembly.GetExecutingAssembly().CodeBase).Path));
        #endregion Private Static Fields

        #region Internal Static Methods
        internal static bool IsUriExists(string uri)
        {
            try
            {
                // Check if file exists first.
                if (File.Exists(uri))
                {
                    return true;
                }

                // Best way to test this is to catch the exception.
                return Application.GetResourceStream(new Uri(uri)) != null;
            }
            catch
            {
                return false;
            }
        }
        #endregion Internal Static Methods
    }
}
