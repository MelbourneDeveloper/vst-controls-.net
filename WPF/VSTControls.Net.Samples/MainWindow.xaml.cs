﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Threading;

namespace VSTControls.Net.Samples
{
    public partial class MainWindow : Window
    {
        const int maxLogSize = 200;
        static int controlsLogCount;
        static int samplesLogCount;

        public MainWindow()
        {
            InitializeComponent();

            DataContext = new PitchElement();

            Message.SetMessageCallback(new MessageCallbackDelegate(VSTControlsMessageCallback));
        }

        public void VSTControlsMessageCallback(object[] args)
        {
            // Execute logging in background
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (Action)delegate
            {
                if (controlsLogCount++ > maxLogSize)
                {
                    controlsLogCount = 0;
                    ControlsMessage.Clear();
                }

                string newLine = ControlsMessage.Text.Equals(string.Empty) ? string.Empty : Environment.NewLine;

                ControlsMessage.Text += string.Concat(newLine, 
                                                      newLine, 
                                                      string.Join(Environment.NewLine, args));

                // Custom error handling by error code
                foreach (Object obj in args)
                {
                    foreach (KeyValuePair<Object, string> text in Message.Text)
                    {
                        if (text.Key.GetType().Equals(typeof(Error)) && 
                            string.CompareOrdinal(obj.ToString(), text.Value).Equals(0))
                        {
                            Debug.WriteLine(string.Concat("Error '",
                                                          Enum.GetName(typeof(Error), text.Key),
                                                          "' encountered in VSTControls.Net library:",
                                                          Environment.NewLine,
                                                          string.Join(Environment.NewLine, args)));
                        }
                    }
                }
            });
        }

        private void TheSwitch_IsOnChanged(object sender, RoutedEventArgs args)
        {
            // Capture value closure
            string value = theSwitch.IsOn.ToString();

            // Execute logging in background
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (Action)delegate
            {
                if (samplesLogCount++ > maxLogSize)
                {
                    samplesLogCount = 0;
                    SamplesMessage.Clear();
                }

                string newLine = SamplesMessage.Text.Equals(string.Empty) ? string.Empty : Environment.NewLine;

                SamplesMessage.Text += string.Concat(newLine,
                                                     theSwitch.GetType().ToString(),
                                                     ": ",
                                                     value);

                SamplesMessage.ScrollToEnd();
            });
        }

        private void TheKnob_ValueChanged(object sender, RoutedEventArgs args)
        {
            // Capture value closure
            string value = theKnob.Value.ToString("R");

            // Execute logging in background
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (Action)delegate
            {
                if (samplesLogCount++ > maxLogSize)
                {
                    samplesLogCount = 0;
                    SamplesMessage.Clear();
                }

                string newLine = SamplesMessage.Text.Equals(string.Empty) ? string.Empty : Environment.NewLine;

                SamplesMessage.Text += string.Concat(newLine,
                                                     theKnob.GetType().ToString(),
                                                     ": ",
                                                     value);

                SamplesMessage.ScrollToEnd();
            });
        }

        private void TheSlider_ValueChanged(object sender, RoutedEventArgs args)
        {
            // Capture value closure
            string value = theSlider.Value.ToString();

            // Execute logging in background
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (Action)delegate
            {
                if (samplesLogCount++ > maxLogSize)
                {
                    samplesLogCount = 0;
                    SamplesMessage.Clear();
                }

                string newLine = SamplesMessage.Text.Equals(string.Empty) ? string.Empty : Environment.NewLine;

                SamplesMessage.Text += string.Concat(newLine,
                                                     theSlider.GetType().ToString(),
                                                     ": ",
                                                     value);

                SamplesMessage.ScrollToEnd();
            });
        }
    }
}
