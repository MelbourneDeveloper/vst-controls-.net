﻿namespace MySynth
{
    /// <summary>
    /// Used to convert floats from 0-1 to a reasonable number and back inside synth elements. 
    /// </summary>
    public class RangeConverter
    {
        private float _RangeValue;

        public float RangeUpper { get; set; }
        public float RangeLower { get; set; }

        public RangeConverter(float rangeLower, float rangeUpper, float rangeValue)
        {
            RangeUpper = rangeUpper;
            RangeLower = rangeLower;
            _RangeValue = rangeValue;
        }

        public float SourceValue
        {
            get
            {
                return (_RangeValue - RangeLower) / (RangeUpper - RangeLower);
            }
            set
            {
                _RangeValue = RangeLower + ((RangeUpper - RangeLower) * value);
            }
        }

        public float RangeValue
        {
            get
            {
                return _RangeValue;
            }
        }

    }
}
