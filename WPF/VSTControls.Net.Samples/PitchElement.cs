﻿using MySynth;
using System.ComponentModel;
using System.Diagnostics;

namespace VSTControls.Net.Samples
{
    public class PitchElement : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool _IsOn;
        private RangeConverter _PitchRangerConverter = new RangeConverter(-12, 12, 0);

        public float Pitch
        {
            get
            {
                return _PitchRangerConverter.SourceValue;
            }

            set
            {
                _PitchRangerConverter.SourceValue = value;
                RaisePropertyChanged(nameof(Pitch));
                RaisePropertyChanged(nameof(PitchDisplay));
            }
        }

        public int PitchDisplay
        {
            get
            {
                return (int)_PitchRangerConverter.RangeValue;
            }
            set
            {
                Debug.WriteLine((int)_PitchRangerConverter.RangeValue);
            }
        }

        public bool IsOn
        {
            get
            {
                return _IsOn;
            }

            set
            {
                _IsOn = value;
                RaisePropertyChanged(nameof(IsOn));
                Debug.WriteLine(value.ToString());
            }
        }

        private void RaisePropertyChanged(string v)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(v));
        }
    }
}
