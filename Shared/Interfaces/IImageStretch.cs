﻿using System;

namespace VSTControls.Net
{
    #if !(WINDOWS_UWP)
    [CLSCompliant(true)]
    #endif
    /// <summary>
    /// Interface for image stretch functionality
    /// </summary>
    public interface IImageStretch
    {
        /// <summary>
        /// ImageStretch boolean property:
        /// False:  Original image size
        /// True:   Image stretch to the size of it's parent container
        /// </summary>
        bool ImageStretch { get; set; }
    }
}
