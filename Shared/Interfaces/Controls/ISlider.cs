﻿#region Using
using System;
#if (WINDOWS_UWP)
using Windows.UI.Xaml.Controls;
#else
using System.Windows.Controls;
#endif
#endregion Using

namespace VSTControls.Net
{
    #if !(WINDOWS_UWP)
    [CLSCompliant(true)]
    #endif
    /// <summary>
    /// Interface for Slider functionality
    /// </summary>
    public interface ISlider : IRange
    {
        /// <summary>
        /// BackgroundImage string property:
        /// Uri for slider images. Can be a sprite sheet or the first image of a sequence ex: 'img_000.png'
        /// Individual sprite size must be of square dimensions (width equals height)
        /// </summary>
        string BackgroundImage { get; set; }

        /// <summary>
        /// ForegroundImage string property:
        /// Uri for slider images. Can be a sprite sheet or the first image of a sequence ex: 'img_000.png'
        /// Individual sprite size must be of square dimensions (width equals height)
        /// </summary>
        string ForegroundImage { get; set; }

        /// <summary>
        /// Maximum double property:
        /// Maximum slider value
        /// </summary>
        Orientation Orientation { get; set; }
        
        /// <summary>
        /// ThumbImage string property:
        /// Uri for slider images. Can be a sprite sheet or the first image of a sequence ex: 'img_000.png'
        /// Individual sprite size must be of square dimensions (width equals height)
        /// </summary>
        string ThumbImage { get; set; }
        
        /// <summary>
        /// TickFrequency string property:
        /// Uri for slider images. Can be a sprite sheet or the first image of a sequence ex: 'img_000.png'
        /// Individual sprite size must be of square dimensions (width equals height)
        /// </summary>
        double TickFrequency { get; set; }
    }
}
