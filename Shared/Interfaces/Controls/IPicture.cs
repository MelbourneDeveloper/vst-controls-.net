﻿using System;

namespace VSTControls.Net
{
    #if !(WINDOWS_UWP)
    [CLSCompliant(true)]
    #endif
    /// <summary>
    /// Interface for Picture functionality
    /// </summary>
    public interface IPicture : IImageStretch
    {
        /// <summary>
        /// PictureImage string property:
        /// Uri for picture image
        /// </summary>
        string PictureImage { get; set; }
    }
}
