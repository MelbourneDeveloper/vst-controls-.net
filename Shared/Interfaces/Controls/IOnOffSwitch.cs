﻿#region Using
using System;
#if (WINDOWS_UWP)
using Windows.UI.Xaml;
#else
using System.Windows;
#endif
#endregion Using

namespace VSTControls.Net
{
    #if !(WINDOWS_UWP)
    [CLSCompliant(true)]
    #endif
    /// <summary>
    /// Interface for Knob functionality
    /// </summary>
    public interface IOnOffSwitch : IImageStretch, IIsKeyboardEnabled
    {
        /// <summary>
        /// IsMouseWheelInverted boolean property:
        /// False:  MouseWheel positive delta value or up key sets the switch to 'on  state' and negative delta value or down key to 'off state'
        /// True:   MouseWheel positive delta value or up key sets the switch to 'off state' and negative delta value or down key to 'on  state'
        /// </summary>
        bool IsMouseWheelInverted { get; set; }
        
        /// <summary>
        /// IsOn boolean property:
        /// False:  Switch 'off' position
        /// True:   Switch 'on ' position
        /// </summary>
        bool IsOn { get; set; }
        
        /// <summary>
        /// IsOnChanged event handler is invoked after IsOn boolean value has changed
        /// </summary>
        event RoutedEventHandler IsOnChanged;

        /// <summary>
        /// OffImage string property:
        /// Uri for 'off' state image
        /// </summary>
        string OffImage { get; set; }
            
        /// <summary>
        /// OnImage string property:
        /// Uri for 'on' state image
        /// </summary>
        string OnImage { get; set; }

        /// <summary>
        /// Toggles IsOn property to 'IsOn = true' or 'IsOn = false' state
        /// </summary>
        void Toggle();
    }
}
