﻿using System;

namespace VSTControls.Net
{
    #if !(WINDOWS_UWP)
    [CLSCompliant(true)]
    #endif
    /// <summary>
    /// Interface for Knob functionality
    /// </summary>
    public interface IKnob : IRange
    {
        /// <summary>
        /// CrossoverTreshold string property:
        /// Treshold value required before crossing over min/max point using mouse or pointer interaction
        /// Must be smaller than half the range between minimum and maximum
        /// </summary>
        double CrossoverThreshold { get; set; }
        
        /// <summary>
        /// EndAngle double property:
        /// End position of knob travel in degree relative to 12' oclock
        /// </summary>
        double EndAngle { get; set; }
        
        /// <summary>
        /// KnobImage string property:
        /// Uri for knob images. Can be a sprite sheet or the first image of a sequence ex: 'img_000.png'
        /// Individual sprite size must be of square dimensions (width equals height)
        /// </summary>
        string KnobImage { get; set; }

        /// <summary>
        /// StartAngle double property:
        /// Start position of knob travel in degree relative to 12' oclock
        /// </summary>
        double StartAngle { get; set; }
    }
}
