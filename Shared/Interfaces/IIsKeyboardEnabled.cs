﻿using System;

namespace VSTControls.Net
{
    #if !(WINDOWS_UWP)
    [CLSCompliant(true)]
    #endif
    /// <summary>
    /// Interface for image stretch functionality
    /// </summary>
    public interface IIsKeyboardEnabled
    {
        /// <summary>
        /// IsKeyboardEnabled boolean property:
        /// False:  Keyboard input disabled
        /// True:   Keyboard input enabled
        /// </summary>
        bool IsKeyboardEnabled { get; set; }
    }
}
