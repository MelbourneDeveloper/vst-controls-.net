﻿#region Using
using System;
#if (WINDOWS_UWP)
using Windows.UI.Xaml;
#else
using System.Windows;
#endif
#endregion Using

namespace VSTControls.Net
{
    #if !(WINDOWS_UWP)
    [CLSCompliant(true)]
    #endif
    /// <summary>
    /// Interface for Knob and Slider range functionality
    /// </summary>
    public interface IRange : IIsKeyboardEnabled, IImageStretch
    {
        /// <summary>
        /// Maximum integer property:
        /// Maximum slider value
        /// </summary>
        int IntegerMaximum { get; set; }
        
        /// <summary>
        /// Minimum integer property:
        /// Minimum slider value
        /// </summary>
        int IntegerMinimum { get; set; }

        /// <summary>
        /// Value integer property:
        /// Slider value
        /// </summary>
        int IntegerValue { get; set; }

        /// <summary>
        /// LargeChange double property:
        /// Fixed value change amount for large increase and decrease operations
        /// </summary>
        double LargeChange { get; set; }
        
        /// <summary>
        /// Maximum double property:
        /// Maximum slider value
        /// </summary>
        double Maximum { get; set; }

        /// <summary>
        /// Minimum double property:
        /// Minimum slider value
        /// </summary>
        double Minimum { get; set; }
        
        /// <summary>
        /// Maximum single property:
        /// Maximum slider value
        /// </summary>
        float SingleMaximum { get; set; }

        /// <summary>
        /// Minimum single property:
        /// Minimum slider value
        /// </summary>
        float SingleMinimum { get; set; }

        /// <summary>
        /// Value single property:
        /// Slider value
        /// </summary>
        float SingleValue { get; set; }

        /// <summary>
        /// SmallChange double property:
        /// Fixed value change amount for small increase and decrease operations
        /// </summary>
        double SmallChange { get; set; }

        /// <summary>
        /// Value double property:
        /// Slider value
        /// </summary>
        double Value { get; set; }

        /// <summary>
        /// ValueChanged event handler is invoked after knob value has changed
        /// </summary>
        event RoutedEventHandler ValueChanged;
    }
}
