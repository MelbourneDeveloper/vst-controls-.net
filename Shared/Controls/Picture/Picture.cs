﻿#region Using
#if (WINDOWS_UWP)
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
#else
using System.Windows;
using System.Windows.Controls;
#endif
#endregion Using

namespace VSTControls.Net
{
    /// <summary>
    /// Class for VSTControls.Net Picture control
    /// </summary>
    public sealed partial class Picture : Control, IPicture
    {
        #region Private Fields
        private Bitmap bitmap;
        #endregion Private Fields

        #region Static Constructor
        static Picture()
        {
            Property.Register<Picture, PictureProp>(properties,
                                                    new PropertyChangedCallback(OnPropertyChanged));
        }
        #endregion Static Constructor
    }
}