﻿#region Using
using System.Collections.Generic;
#if (WINDOWS_UWP)
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
#else
using System.Windows;
using System.Windows.Controls;
#endif
#endregion Using

namespace VSTControls.Net
{
    public sealed partial class Picture : Control
    {
        #region Public Properties
        /// <summary>
        /// ImageStretch boolean property:
        /// False:  Original image size
        /// True:   Image stretch to the size of it's parent container
        /// </summary>
        public bool ImageStretch
        {
            get { return Get<bool>(PictureProp.ImageStretch); }
            set { Set<bool>(PictureProp.ImageStretch, value); }
        }

        /// <summary>
        /// PictureImage string property:
        /// Uri for picture image
        /// </summary>
        public string PictureImage
        {
            get { return Get<string>(PictureProp.PictureImage); }
            set { Set<string>(PictureProp.PictureImage, value); }
        }        
        #endregion Public Properties

        #region Private Static Fields
        private static readonly Dictionary<PictureProp, DependencyProperty> properties = new Dictionary<PictureProp, DependencyProperty>(Property.PicturePropCount);
        #endregion Private Static Fields

        #region Private Methods
        private PropType Get<PropType>(PictureProp prop)
        {
            return Property.Get<PictureProp, PropType>(this, 
                                                       bitmap.Lock, 
                                                       properties, 
                                                       prop);
        }

        private void Set<PropType>(PictureProp prop, object value)
        {
            Property.Set<PictureProp>(this,
                                      bitmap.Lock,
                                      properties,
                                      prop,
                                      value);
        }
        #endregion Private Methods
    }
}