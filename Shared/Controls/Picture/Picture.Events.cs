﻿#region Using
#if (WINDOWS_UWP)
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
#else
using System.Windows;
using System.Windows.Controls;
#endif
#endregion Using

namespace VSTControls.Net
{
    public sealed partial class Picture : Control
    {
        #region Private Static Methods
        private static void MessagePump<PropType>(DependencyObject sender, object newValue, object prop)
        {
            Picture @this = (Picture)sender;

            if (@this == null ||
                prop == null)
            {
                return;
            }

            lock (@this.bitmap.Lock)
            {
                // Invalidate rendering
                @this.InvalidateMeasure();
                @this.InvalidateVisual();
            }
        }
        #endregion Private Static Methods
    }
}