﻿#region Using
using System;
#if (WINDOWS_UWP)
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
#else
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Controls;
#endif
#endregion Using

namespace VSTControls.Net
{
    public sealed partial class Slider
    {
        #region Public Events
        /// <summary>
        /// ValueChanged event handler is invoked after slider value has changed
        /// </summary>
        public event RoutedEventHandler ValueChanged;
        #endregion Public Events

        #region Private Static Methods
        private static void MessagePump<PropType>(DependencyObject sender, object newValue, object prop)
        {
            const double half = 0.5d;
            const double rightAngle = 90d;
            Slider @this = (Slider)sender;

            if (@this == null ||
                prop == null)
            {
                return;
            }

            lock (@this.bitmap.Lock)
            {
                if (prop.Equals(SliderProp.Orientation))
                {
                    // Change orientation
                    bool isVertical = ((Orientation)newValue).Equals(Orientation.Vertical);
                    @this.RenderTransformOrigin = isVertical ? new Point() : new Point(half, half);

                    #if (WINDOWS_UWP)
                    @this.RenderTransform = isVertical ? null : new RotateTransform()
                    {
                        Angle = rightAngle
                    };
                    #else
                    @this.LayoutTransform = isVertical ? null : new RotateTransform(rightAngle);

                    if (@this.LayoutTransform != null)
                    {
                        @this.LayoutTransform.Freeze();
                    }
                    #endif
                }
                else if (prop.Equals(SliderProp.TickFrequency))
                {
                    // Refresh interval quantization
                    @this.Value = @this.Value;
                }
                else if (prop.Equals(SliderProp.IntegerMinimum) ||
                         prop.Equals(SliderProp.SingleMinimum))
                {
                    @this.Minimum = (double)Convert.ChangeType(newValue, typeof(double));
                }
                else if (prop.Equals(SliderProp.IntegerMaximum) ||
                         prop.Equals(SliderProp.SingleMaximum))
                {
                    @this.Maximum = (double)Convert.ChangeType(newValue, typeof(double));
                }
                else if (prop.Equals(SliderProp.IntegerValue) ||
                         prop.Equals(SliderProp.SingleValue))
                {
                    @this.Value = (double)Convert.ChangeType(newValue, typeof(double));
                }

                if (!prop.Equals(SliderProp.LargeChange) &&
                    !prop.Equals(SliderProp.SmallChange))
                {
                    // Invalidate rendering
                    @this.InvalidateMeasure();
                    @this.InvalidateVisual();
                }
            }
        }
        #endregion Private Static Methods

        #region Private Methods
        private void OnKeyDown(Key key)
        {
            lock (bitmap.Lock)
            {
                Value = RangeBase.OnKeyDown(Value,
                                            Minimum,
                                            Maximum,
                                            SmallChange,
                                            LargeChange,
                                            key);
            }
        }

        private void OnMouseDown(Point mousePosition)
        {
            lock (bitmap.Lock)
            {
                Value = ConvertMousePositionYToValue(mousePosition.Y);
            }
        }

        private void OnMouseMove(Point mousePosition)
        {
            lock (bitmap.Lock)
            {
                Value = ConvertMousePositionYToValue(mousePosition.Y);
            }
        }
        
        private void OnMouseWheel(int delta)
        {
            lock (bitmap.Lock)
            {
                Value = RangeBase.OnMouseWheel(Value, delta, SmallChange);
            }
        }

        private double ConvertMousePositionYToValue(double mousePositionY)
        {
            // Invert minimum / maximum
            return UtilMath.ConvertRange(mousePositionY,
                                         0d,
                                         bitmap.rectangle.Height,
                                         Maximum,
                                         Minimum);
        }
        #endregion Private Methods
    }
}