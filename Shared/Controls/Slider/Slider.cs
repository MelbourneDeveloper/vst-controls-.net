﻿#region Using
using System;
#if (WINDOWS_UWP)
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
#else
using System.Windows;
using System.Windows.Controls;
#endif
#endregion Using

namespace VSTControls.Net
{
    #if !(WINDOWS_UWP)
    [CLSCompliant(true)]
    #endif
    /// <summary>
    /// Class for VSTControls.Net Slider control
    /// </summary>
    public sealed partial class Slider : Control, ISlider
    {
        #region Private Fields
        private Bitmap bitmap;
        #endregion Private Fields

        #region Static Constructor
        static Slider()
        {
            Property.Register<Slider, SliderProp>(properties, new PropertyChangedCallback(OnPropertyChanged));
        }
        #endregion Static Constructor
    }
}