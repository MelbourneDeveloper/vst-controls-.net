﻿#region Using
using System;
using System.Collections.Generic;
#if (WINDOWS_UWP)
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
#else
using System.Windows;
using System.Windows.Controls;
#endif
#endregion Using

namespace VSTControls.Net
{
    public sealed partial class Slider
    {
        #region Public Properties
        /// <summary>
        /// BackgroundImage string property:
        /// Uri for slider images. Can be a sprite sheet or the first image of a sequence ex: 'img_000.png'
        /// Individual sprite size must be of square dimensions (width equals height)
        /// </summary>
        public string BackgroundImage
        {
            get { return Get<string>(SliderProp.BackgroundImage); }
            set { Set<string>(SliderProp.BackgroundImage, value); }
        }

        /// <summary>
        /// ForegroundImage string property:
        /// Uri for slider images. Can be a sprite sheet or the first image of a sequence ex: 'img_000.png'
        /// Individual sprite size must be of square dimensions (width equals height)
        /// </summary>
        public string ForegroundImage
        {
            get { return Get<string>(SliderProp.ForegroundImage); }
            set { Set<string>(SliderProp.ForegroundImage, value); }
        }

        /// <summary>
        /// ImageStretch boolean property:
        /// False:  Original image size
        /// True:   Image stretch to the size of it's parent container
        /// </summary>
        public bool ImageStretch
        {
            get { return Get<bool>(SliderProp.ImageStretch); }
            set { Set<bool>(SliderProp.ImageStretch, value); }
        }

        /// <summary>
        /// Maximum integer property:
        /// Maximum slider value
        /// </summary>
        public int IntegerMaximum
        {
            get { return (int)Maximum; }
            set { Maximum = value; }
        }

        /// <summary>
        /// Minimum integer property:
        /// Minimum slider value
        /// </summary>
        public int IntegerMinimum
        {
            get { return (int)Minimum; }
            set { Minimum = value; }
        }

        /// <summary>
        /// Value integer property:
        /// Slider value
        /// </summary>
        public int IntegerValue
        {
            get { return (int)Value; }
            set { Value = value; }
        }

        /// <summary>
        /// IsKeyboardEnabled boolean property:
        /// False:  Keyboard input disabled
        /// True:   Keyboard input enabled
        /// </summary>
        public bool IsKeyboardEnabled
        {
            get { return Get<bool>(SliderProp.IsKeyboardEnabled); }
            set { Set<bool>(SliderProp.IsKeyboardEnabled, value); }
        }

        /// <summary>
        /// LargeChange double property:
        /// Fixed value change amount for large increase and decrease operations
        /// </summary>
        public double LargeChange
        {
            get { return Get<double>(SliderProp.LargeChange); }
            set { Set<double>(SliderProp.LargeChange, value); }
        }

        /// <summary>
        /// Maximum double property:
        /// Maximum slider value
        /// </summary>
        public double Maximum
        {
            get { return Get<double>(SliderProp.Maximum); }
            set { Set<double>(SliderProp.Maximum, value); }
        }

        /// <summary>
        /// Minimum double property:
        /// Minimum slider value
        /// </summary>
        public double Minimum
        {
            get { return Get<double>(SliderProp.Minimum); }
            set { Set<double>(SliderProp.Minimum, value); }
        }

        /// <summary>
        /// Maximum double property:
        /// Maximum slider value
        /// </summary>
        public Orientation Orientation
        {
            get { return Get<Orientation>(SliderProp.Orientation); }
            set { Set<Orientation>(SliderProp.Orientation, value); }
        }
        
        /// <summary>
        /// Maximum single property:
        /// Maximum slider value
        /// </summary>
        public float SingleMaximum
        {
            get { return (float)Maximum; }
            set { Maximum = value; }
        }

        /// <summary>
        /// Minimum single property:
        /// Minimum slider value
        /// </summary>
        public float SingleMinimum
        {
            get { return (float)Minimum; }
            set { Minimum = value; }
        }

        /// <summary>
        /// Value single property:
        /// Slider value
        /// </summary>
        public float SingleValue
        {
            get { return (float)Value; }
            set { Value = value; }
        }

        /// <summary>
        /// SmallChange double property:
        /// Fixed value change amount for small increase and decrease operations
        /// </summary>
        public double SmallChange
        {
            get { return Get<double>(SliderProp.SmallChange); }
            set { Set<double>(SliderProp.SmallChange, value); }
        }

        /// <summary>
        /// ThumbImage string property:
        /// Uri for slider images. Can be a sprite sheet or the first image of a sequence ex: 'img_000.png'
        /// Individual sprite size must be of square dimensions (width equals height)
        /// </summary>
        public string ThumbImage
        {
            get { return Get<string>(SliderProp.ThumbImage); }
            set { Set<string>(SliderProp.ThumbImage, value); }
        }

        /// <summary>
        /// TickFrequency string property:
        /// Uri for slider images. Can be a sprite sheet or the first image of a sequence ex: 'img_000.png'
        /// Individual sprite size must be of square dimensions (width equals height)
        /// </summary>
        public double TickFrequency
        {
            get { return Get<double>(SliderProp.TickFrequency); }
            set { Set<double>(SliderProp.TickFrequency, value); }
        }

        /// <summary>
        /// Value double property:
        /// Slider value
        /// </summary>
        public double Value
        {
            get { return Get<double>(SliderProp.Value); }
            set
            {
                lock (bitmap.Lock)
                {
                    double oldValue = Value;
                    double newValue = ValidateSetProperty<SliderProp, double>(SliderProp.Value, value);

                    if (!oldValue.Equals(newValue))
                    {
                        Set<double>(SliderProp.Value, newValue);
                        
                        if (ValueChanged != null)
                        {

                            System.Diagnostics.Debug.WriteLine(oldValue + " - " + newValue);
                            ValueChanged(this, new RoutedEventArgs());
                        }
                    }
                }
            }
        }
        #endregion Public Properties
        
        #region Private Static Fields
        private static readonly Dictionary<SliderProp, DependencyProperty> properties = new Dictionary<SliderProp, DependencyProperty>(Property.SliderPropCount);
        #endregion Private Static Fields

        #region Private Methods
        private PropType Get<PropType>(SliderProp prop)
        {
            return Property.Get<SliderProp, PropType>(this,
                                                      this.bitmap.Lock,
                                                      properties,
                                                      prop);
        }

        private void Set<PropType>(SliderProp prop, object value)
        {
            Property.Set<SliderProp>(this,
                                     this.bitmap.Lock,
                                     properties,
                                     prop,
                                     ValidateSetProperty<SliderProp, PropType>(prop, value));
        }

        private PropType ValidateSetProperty<Prop, PropType>(SliderProp prop, object value)
        {
            if (prop.Equals(SliderProp.LargeChange))
            {
                double newLargeChange = (double)value;
                double tickFrequency = TickFrequency;
                double range = Maximum - Minimum;

                if (newLargeChange > range)
                {
                    value = range;
                }

                if (newLargeChange < tickFrequency)
                {
                    value = tickFrequency;
                }
            }
            else if (prop.Equals(SliderProp.Maximum))
            {
                double newMaximum = (double)value;
                double maximum = Maximum;

                if (newMaximum < maximum)
                {
                    value = maximum;
                }
            }
            else if (prop.Equals(SliderProp.Minimum))
            {
                double newMinimum = (double)value;
                double minimum = Minimum;

                if (newMinimum > minimum)
                {
                    value = minimum;
                }
            }
            else if (prop.Equals(SliderProp.SmallChange))
            {
                double newSmallChange = (double)value;
                double tickFrequency = TickFrequency;
                double range = Maximum - Minimum;

                if (newSmallChange > range)
                {
                    value = range;
                }

                if (newSmallChange < tickFrequency)
                {
                    value = tickFrequency;
                }
            }
            else if (prop.Equals(SliderProp.TickFrequency))
            {
                double newTickFrequency = (double)value;
                double range = Maximum - Minimum;
                double smallChange = SmallChange;

                if (newTickFrequency > range)
                {
                    value = range;
                }

                if (newTickFrequency < smallChange)
                {
                    value = smallChange;
                }
            }
            else if (prop.Equals(SliderProp.Value))
            {
                double maximum = Maximum;
                double minimum = Minimum;
                double newValue = UtilMath.ClampValue((double)value, minimum, maximum);
                double tickFrequency = TickFrequency;

                // If not divide by zero and is inside exclusive interval
                if (!tickFrequency.Equals(0d) &&
                    !newValue.Equals(minimum) &&
                    !newValue.Equals(maximum))
                {
                    newValue = UtilMath.ClampValue(tickFrequency * Math.Round(newValue / tickFrequency),
                                                   minimum,
                                                   maximum);
                }

                value = newValue;
            }

            return (PropType)value;
        }
        #endregion Private Methods
    }
}