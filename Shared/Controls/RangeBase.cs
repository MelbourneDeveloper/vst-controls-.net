﻿using System.Windows.Input;

namespace VSTControls.Net
{
    internal static class RangeBase
    {
        public static double OnKeyDown(double oldValue, double minimum, double maximum, double smallChange, double largeChange, Key key)
        {
            double newValue = oldValue;

            if (key.Equals(Key.Down) ||
                key.Equals(Key.Left))
            {
                newValue = oldValue - smallChange;
            }
            else if (key.Equals(Key.Up) ||
                     key.Equals(Key.Right))
            {
                newValue = oldValue + smallChange;
            }
            else if (key.Equals(Key.PageDown))
            {
                newValue = oldValue - largeChange;
            }
            else if (key.Equals(Key.PageUp))
            {
                newValue = oldValue + largeChange;
            }
            else if (key.Equals(Key.Home))
            {
                newValue = minimum;
            }
            else if (key.Equals(Key.End))
            {
                newValue = maximum;
            }

            return newValue;
        }

        public static double OnMouseWheel(double oldValue, int delta, double smallChange)
        {
            return oldValue + (delta > 0 ? smallChange : -smallChange);
        }
    }
}
