﻿#region Using
using System;
using System.Collections.Generic;
#if (WINDOWS_UWP)
using Windows.UI.Xaml;
#else
using System.Windows;
#endif
#endregion Using

namespace VSTControls.Net
{
    public partial class OnOffSwitch
    {
        #region Public Properties
        #if !(WINDOWS_UWP)
        [CLSCompliant(true)]
        #endif
        /// <summary>
        /// ImageStretch boolean property:
        /// False:  Original image size
        /// True:   Image stretch to the size of it's parent container
        /// </summary>
        public bool ImageStretch
        {
            get { return Get<bool>(OnOffSwitchProp.ImageStretch); }
            set { Set<bool>(OnOffSwitchProp.ImageStretch, value); }
        }

        #if !(WINDOWS_UWP)
        [CLSCompliant(true)]
        #endif
        /// <summary>
        /// IsKeyboardEnabled boolean property:
        /// False:  Keyboard input disabled
        /// True:   Keyboard input enabled
        /// </summary>
        public bool IsKeyboardEnabled
        {
            get { return Get<bool>(OnOffSwitchProp.IsKeyboardEnabled); }
            set { Set<bool>(OnOffSwitchProp.IsKeyboardEnabled, value); }
        }

        #if !(WINDOWS_UWP)
        [CLSCompliant(true)]
        #endif
        /// <summary>
        /// IsMouseWheelInverted boolean property:
        /// False:  MouseWheel positive delta value or up key sets the switch to 'on  state' and negative delta value or down key to 'off state'
        /// True:   MouseWheel positive delta value or up key sets the switch to 'off state' and negative delta value or down key to 'on  state'
        /// </summary>
        public bool IsMouseWheelInverted
        {
            get { return Get<bool>(OnOffSwitchProp.IsMouseWheelInverted); }
            set { Set<bool>(OnOffSwitchProp.IsMouseWheelInverted, value); }
        }

        #if !(WINDOWS_UWP)
        [CLSCompliant(true)]
        #endif
        /// <summary>
        /// IsOn boolean property:
        /// False:  Switch 'off' position
        /// True:   Switch 'on ' position
        /// </summary>
        public bool IsOn
        {
            get 
            { 
                return Get<bool>(OnOffSwitchProp.IsOn); 
            }
            set
            {
                bool oldIsOn = IsOn;

                if (!oldIsOn.Equals(value))
                {
                    Set<bool>(OnOffSwitchProp.IsOn, value);
                    IsOnChanged?.Invoke(this, new RoutedEventArgs());
                }
            }
        }

        #if !(WINDOWS_UWP)
        [CLSCompliant(true)]
        #endif
        /// <summary>
        /// OffImage string property:
        /// Uri for 'off' state image
        /// </summary>
        public string OffImage
        {
            get { return Get<string>(OnOffSwitchProp.OffImage); }
            set { Set<string>(OnOffSwitchProp.OffImage, value); }
        }
        
        #if !(WINDOWS_UWP)
        [CLSCompliant(true)]
        #endif
        /// <summary>
        /// OnImage string property:
        /// Uri for 'on' state image
        /// </summary>
        public string OnImage
        {
            get { return Get<string>(OnOffSwitchProp.OnImage); }
            set { Set<string>(OnOffSwitchProp.OnImage, value); }
        }
        #endregion Public Properties

        #region Private Static Fields
        private static readonly Dictionary<OnOffSwitchProp, DependencyProperty> properties = new Dictionary<OnOffSwitchProp, DependencyProperty>(Property.OnOffSwitchPropCount);
        #endregion Private Static Fields

        #region Private Methods
        private PropType Get<PropType>(OnOffSwitchProp prop)
        {
            return Property.Get<OnOffSwitchProp, PropType>(this,
                                                           this.bitmap.Lock,
                                                           properties,
                                                           prop);
        }

        private void Set<PropType>(OnOffSwitchProp prop, object value)
        {
            Property.Set<OnOffSwitchProp>(this,
                                          this.bitmap.Lock,
                                          properties,
                                          prop,
                                          value);
        }
        #endregion Private Methods
    }
}