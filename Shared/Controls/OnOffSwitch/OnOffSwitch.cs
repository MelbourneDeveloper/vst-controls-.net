﻿#region Using
using System;
using System.Collections.Generic;
#if (WINDOWS_UWP)
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
#else
using System.Windows;
using System.Windows.Controls;
#endif
#endregion Using

namespace VSTControls.Net
{
    /// <summary>
    /// Class for VSTControls.Net OnOffSwitch control
    /// </summary>
    public partial class OnOffSwitch : Control, IOnOffSwitch
    {
        #region Private Enums
        private enum State
        {
            Off,
            On
        }
        #endregion Private Enums

        #region Private Static Fields
        private static readonly int StateCount = Enum.GetValues(typeof(State)).Length;

        private static readonly Dictionary<object, State> ObjectToState = new Dictionary<object, State>(StateCount * 2)
        {
            { OnOffSwitchProp.OffImage, State.Off },
            { OnOffSwitchProp.OnImage, State.On },
            { false, State.Off },
            { true, State.On }
        };
        #endregion Private Static Fields

        #region Private Fields
        private readonly Dictionary<State, string> StateToImagePath = new Dictionary<State, string>(StateCount);
        private Bitmap bitmap;
        #endregion Private Fields

        #region Static Constructor
        static OnOffSwitch()
        {
            Property.Register<OnOffSwitch, OnOffSwitchProp>(properties, new PropertyChangedCallback(OnPropertyChanged));
        }
        #endregion Static Constructor

        #region Public Methods
        /// <summary>
        /// Toggles IsOn property to 'IsOn = true' or 'IsOn = false' state
        /// </summary>
        public void Toggle()
        {
            IsOn = !IsOn;
        }
        #endregion Public Methods
    }
}