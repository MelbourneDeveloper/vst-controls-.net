﻿#if(WINDOWS_UWP)
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
#else
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
#endif

namespace VSTControls.Net
{
    public class OnOffSwitch : ButtonBase
    {
        #region Dependency Properties
        public static readonly DependencyProperty IsOnProperty = DependencyProperty.Register(nameof(IsOn), typeof(bool), typeof(OnOffSwitch), null);
        public static readonly DependencyProperty OnImageSourceProperty = DependencyProperty.Register(nameof(OnImageSource), typeof(ImageSource), typeof(OnOffSwitch), new PropertyMetadata(null, new PropertyChangedCallback(OnOnImageChanged)));
        public static readonly DependencyProperty OffImageSourceProperty = DependencyProperty.Register(nameof(OffImageSource), typeof(ImageSource), typeof(OnOffSwitch), new PropertyMetadata(null, new PropertyChangedCallback(OnOffImageChanged)));
        #endregion

        #region Dependency Property Callbacks
        private static void OnOnImageChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (OnOffSwitch)d;
            control.OnImageSource = (ImageSource)e.NewValue;
        }

        private static void OnOffImageChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (OnOffSwitch)d;
            control.OffImageSource = (ImageSource)e.NewValue;
        }
        #endregion

        #region Private Static Fields
        private static Style _DefaultImageStyle;
        #endregion

        #region Static Constructor
        static OnOffSwitch()
        {
            _DefaultImageStyle = new Style();
            _DefaultImageStyle.TargetType = typeof(Image);
            _DefaultImageStyle.Setters.Add(new Setter(Image.StretchProperty, Stretch.None));
        }
        #endregion

        #region Fields
        private Image _OnImage = new Image { HorizontalAlignment = HorizontalAlignment.Stretch, VerticalAlignment = VerticalAlignment.Stretch };
        private Image _OffImage = new Image { HorizontalAlignment = HorizontalAlignment.Stretch, VerticalAlignment = VerticalAlignment.Stretch };
        #endregion

        #region Events
        public event RoutedEventHandler IsOnChanged;
        #endregion

        #region Public Properties
        public bool IsOn
        {
            get
            {
                return (bool)GetValue(IsOnProperty);
            }
            set
            {
                SetValue(IsOnProperty, value);
                Toggle();
            }
        }

        public ImageSource OnImageSource
        {
            get
            {
                return (ImageSource)GetValue(OnImageSourceProperty);
            }
            set
            {
                SetValue(OnImageSourceProperty, value);
                _OnImage.Source = value;
            }
        }

        public ImageSource OffImageSource
        {
            get
            {
                return (ImageSource)GetValue(OffImageSourceProperty);
            }
            set
            {
                SetValue(OffImageSourceProperty, value);
                _OffImage.Source = value;
            }
        }

        public Style ImageStyle
        {
            get
            {
                return _OffImage.Style;
            }
            set
            {
                _OffImage.Style = value;
                _OnImage.Style = value;
            }
        }
        #endregion

        #region Constructor
        public OnOffSwitch()
        {
            HorizontalContentAlignment = HorizontalAlignment.Stretch;
            VerticalContentAlignment = VerticalAlignment.Stretch;
            ImageStyle = _DefaultImageStyle;
            Toggle();
        }
        #endregion

        #region Protected Overrides
#if(WINDOWS_UWP)
        protected override void OnTapped(TappedRoutedEventArgs e)
        {
            base.OnTapped(e);
            IsOn = !IsOn;
            IsOnChanged?.Invoke(this, new RoutedEventArgs());
        }
#else
        protected override void OnClick()
        {
            base.OnClick();
            IsOn = !IsOn;
            IsOnChanged?.Invoke(this, new RoutedEventArgs());
        }
#endif
        #endregion

        #region Private Methods
        private void Toggle()
        {
            Content = IsOn ? _OnImage : _OffImage;
        }
        #endregion
    }
}
