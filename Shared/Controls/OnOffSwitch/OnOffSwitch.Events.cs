﻿#region Using
using System;
#if (WINDOWS_UWP)
using Windows.Foundation;
using Windows.UI.Xaml;
#else
using System.Windows;
using System.Windows.Input;
#endif
#endregion Using

namespace VSTControls.Net
{
    public partial class OnOffSwitch
    {
        #region Public Events
        #if !(WINDOWS_UWP)
        [CLSCompliant(true)]
        #endif
        /// <summary>
        /// IsOnChanged event handler is invoked after IsOn boolean value has changed
        /// </summary>
        public event RoutedEventHandler IsOnChanged;
        #endregion Public Events

        #region Private Static Methods
        private static void MessagePump<PropType>(DependencyObject sender, object newValue, object prop)
        {
            OnOffSwitch @this = (OnOffSwitch)sender;

            if (@this == null ||
                prop == null)
            {
                return;
            }

            lock (@this.bitmap.Lock)
            {
                if (ObjectToState.ContainsKey(prop))
                {
                    // Refresh state image
                    @this.StateToImagePath[ObjectToState[prop]] = UtilUri.BuildUri(newValue as string);
                }

                if (!prop.Equals(OnOffSwitchProp.IsKeyboardEnabled) &&
                    !prop.Equals(OnOffSwitchProp.IsMouseWheelInverted))
                {
                    // Invalidate rendering
                    @this.InvalidateMeasure();
                    @this.InvalidateVisual();
                }
            }
        }
        #endregion Private Static Methods

        #region Private Methods
        private void OnKeyUp(Key key)
        {
            lock (bitmap.Lock)
            {
                if (key.Equals(Key.Space) ||
                    key.Equals(Key.Enter))
                {
                    Toggle();
                }
                else if (key.Equals(Key.Down))
                {
                    IsOn = IsMouseWheelInverted;
                }
                else if (key.Equals(Key.Up))
                {
                    IsOn = !IsMouseWheelInverted;
                }
            }
        }

        private void OnMouseUp(Point mousePosition)
        {
            lock (bitmap.Lock)
            {
                if (bitmap.rectangle.Contains(mousePosition))
                {
                    Toggle();
                }
            }
        }

        private void OnMouseWheel(int delta)
        {
            lock (bitmap.Lock)
            {
                IsOn = IsMouseWheelInverted ? delta < 0 : delta > 0;
            }
        }
        #endregion Private Methods
    }
}