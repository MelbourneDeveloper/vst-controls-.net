﻿#region Using
using System;
#if (WINDOWS_UWP)
using Windows.Foundation;
using Windows.UI.Xaml;
#else
using System.Windows;
using System.Windows.Input;
#endif
#endregion Using

namespace VSTControls.Net
{
    public sealed partial class Knob
    {
        #region Public Events
        /// <summary>
        /// ValueChanged event handler is invoked after knob value has changed
        /// </summary>
        public event RoutedEventHandler ValueChanged;
        #endregion Public Events

        #region Private Static Methods
        private static void MessagePump<PropType>(DependencyObject sender, object newValue, object prop)
        {
            Knob @this = (Knob)sender;

            if (@this == null || 
                prop == null)
            {
                return;
            }

            lock (@this.bitmap.Lock)
            {
                if (prop.Equals(SliderProp.IntegerMinimum) ||
                    prop.Equals(SliderProp.SingleMinimum))
                {
                    @this.Minimum = (double)Convert.ChangeType(newValue, typeof(double));
                }
                else if (prop.Equals(SliderProp.IntegerMaximum) ||
                         prop.Equals(SliderProp.SingleMaximum))
                {
                    @this.Maximum = (double)Convert.ChangeType(newValue, typeof(double));
                }
                else if (prop.Equals(SliderProp.IntegerValue) ||
                         prop.Equals(SliderProp.SingleValue))
                {
                    @this.Value = (double)Convert.ChangeType(newValue, typeof(double));
                }

                if (!prop.Equals(KnobProp.CrossoverThreshold) &&
                    !prop.Equals(KnobProp.IsKeyboardEnabled) &&
                    !prop.Equals(KnobProp.LargeChange) &&
                    !prop.Equals(KnobProp.SmallChange))
                {
                    // Invalidate rendering
                    @this.InvalidateMeasure();
                    @this.InvalidateVisual();
                }
            }
        }
        #endregion Private Static Methods

        #region Private Methods
        private void OnKeyDown(Key key)
        {
            lock (bitmap.Lock)
            {
                Value = RangeBase.OnKeyDown(Value,
                                            Minimum,
                                            Maximum,
                                            SmallChange,
                                            LargeChange,
                                            key);
            }
        }

        private void OnMouseDown(Point mousePosition)
        {
            lock (bitmap.Lock)
            {
                Value = ConvertAngleToValue(mousePosition);
            }
        }

        private void OnMouseMove(Point mousePosition)
        {
            lock (bitmap.Lock)
            {
                Value = UtilMath.ClampValue(Value,
                                            ConvertAngleToValue(mousePosition),
                                            Minimum,
                                            Maximum,
                                            CrossoverThreshold);
            }
        }
        
        private void OnMouseWheel(int delta)
        {
            lock (bitmap.Lock)
            {
                Value = RangeBase.OnMouseWheel(Value, delta, SmallChange);
            }
        }
        #endregion Private Methods
    }
}