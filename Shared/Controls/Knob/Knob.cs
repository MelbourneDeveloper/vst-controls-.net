﻿#region Using
#if (WINDOWS_UWP)
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
#else
using System.Windows;
using System.Windows.Controls;
#endif
#endregion Using

namespace VSTControls.Net
{
    /// <summary>
    /// Class for VSTControls.Net Knob control
    /// </summary>
    public sealed partial class Knob : Control, IKnob
    {
        #region Private Fields
        private Bitmap bitmap;
        #endregion Private Fields

        #region Static Constructor
        static Knob()
        {
            Property.Register<Knob, KnobProp>(properties, new PropertyChangedCallback(OnPropertyChanged));
        }
        #endregion Static Constructor
    }
}