﻿#region Using
using System.Collections.Generic;
#if (WINDOWS_UWP)
using Windows.UI.Xaml;
#else
using System.Windows;
#endif
#endregion Using

namespace VSTControls.Net
{
    public sealed partial class Knob
    {
        #region Public Properties
        /// <summary>
        /// CrossoverTreshold string property:
        /// Treshold value required before crossing over min/max point using mouse or pointer interaction
        /// Must be smaller than half the range between minimum and maximum
        /// </summary>
        public double CrossoverThreshold
        {
            get { return Get<double>(KnobProp.CrossoverThreshold); }
            set { Set<double>(KnobProp.CrossoverThreshold, value); }
        }

        /// <summary>
        /// EndAngle double property:
        /// End position of knob travel in degree relative to 12' oclock
        /// </summary>
        public double EndAngle
        {
            get { return Get<double>(KnobProp.EndAngle); }
            set { Set<double>(KnobProp.EndAngle, value); }
        }

        /// <summary>
        /// ImageStretch boolean property:
        /// False:  Original image size
        /// True:   Image stretch to the size of it's parent container
        /// </summary>
        public bool ImageStretch
        {
            get { return Get<bool>(KnobProp.ImageStretch); }
            set { Set<bool>(KnobProp.ImageStretch, value); }
        }

        /// <summary>
        /// Maximum integer property:
        /// Maximum slider value
        /// </summary>
        public int IntegerMaximum
        {
            get { return (int)Maximum; }
            set { Maximum = value; }
        }

        /// <summary>
        /// Minimum integer property:
        /// Minimum slider value
        /// </summary>
        public int IntegerMinimum
        {
            get { return (int)Minimum; }
            set { Minimum = value; }
        }

        /// <summary>
        /// Value integer property:
        /// Slider value
        /// </summary>
        public int IntegerValue
        {
            get { return (int)Value; }
            set { Value = value; }
        }

        /// <summary>
        /// IsKeyboardEnabled boolean property:
        /// False:  Keyboard input disabled
        /// True:   Keyboard input enabled
        /// </summary>
        public bool IsKeyboardEnabled
        {
            get { return Get<bool>(KnobProp.IsKeyboardEnabled); }
            set { Set<bool>(KnobProp.IsKeyboardEnabled, value); }
        }

        /// <summary>
        /// KnobImage string property:
        /// Uri for knob images. Can be a sprite sheet or the first image of a sequence ex: 'img_000.png'
        /// Individual sprite size must be of square dimensions (width equals height)
        /// </summary>
        public string KnobImage
        {
            get { return Get<string>(KnobProp.KnobImage); }
            set { Set<string>(KnobProp.KnobImage, value); }
        }

        /// <summary>
        /// LargeChange double property:
        /// Fixed value change amount for large increase and decrease operations
        /// </summary>
        public double LargeChange
        {
            get { return Get<double>(KnobProp.LargeChange); }
            set { Set<double>(KnobProp.LargeChange, value); }
        }

        /// <summary>
        /// Maximum double property:
        /// Maximum knob value
        /// </summary>
        public double Maximum
        {
            get { return Get<double>(KnobProp.Maximum); }
            set { Set<double>(KnobProp.Maximum, value); }
        }

        /// <summary>
        /// Minimum double property:
        /// Minimum knob value
        /// </summary>
        public double Minimum
        {
            get { return Get<double>(KnobProp.Minimum); }
            set { Set<double>(KnobProp.Minimum, value); }
        }

        /// <summary>
        /// Maximum single property:
        /// Maximum slider value
        /// </summary>
        public float SingleMaximum
        {
            get { return (float)Maximum; }
            set { Maximum = value; }
        }

        /// <summary>
        /// Minimum single property:
        /// Minimum slider value
        /// </summary>
        public float SingleMinimum
        {
            get { return (float)Minimum; }
            set { Minimum = value; }
        }

        /// <summary>
        /// Value single property:
        /// Slider value
        /// </summary>
        public float SingleValue
        {
            get { return (float)Value; }
            set { Value = value; }
        }
        
        /// <summary>
        /// SmallChange double property:
        /// Fixed value change amount for small increase and decrease operations
        /// </summary>
        public double SmallChange
        {
            get { return Get<double>(KnobProp.SmallChange); }
            set { Set<double>(KnobProp.SmallChange, value); }
        }

        /// <summary>
        /// StartAngle double property:
        /// Start position of knob travel in degree relative to 12' oclock
        /// </summary>
        public double StartAngle
        {
            get { return Get<double>(KnobProp.StartAngle); }
            set { Set<double>(KnobProp.StartAngle, value); }
        }

        /// <summary>
        /// Value property:
        /// Knob value
        /// </summary>
        public double Value
        {
            get { return Get<double>(KnobProp.Value); }
            set
            {
                lock (bitmap.Lock)
                {
                    double oldValue = Value;
                    double newValue = ValidateSetProperty<KnobProp, double>(KnobProp.Value, value);

                    if (!oldValue.Equals(newValue))
                    {
                        Set<double>(KnobProp.Value, newValue);

                        if (ValueChanged != null)
                        {
                            ValueChanged(this, new RoutedEventArgs());
                        }
                    }
                }
            }
        }
        #endregion Public Properties

        #region Private Static Fields
        private static readonly Dictionary<KnobProp, DependencyProperty> properties = new Dictionary<KnobProp, DependencyProperty>(Property.KnobPropCount);
        #endregion Private Static Fields

        #region Private Methods
        private PropType Get<PropType>(KnobProp prop)
        {
            return Property.Get<KnobProp, PropType>(this,
                                                    this.bitmap.Lock,
                                                    properties,
                                                    prop);
        }

        private void Set<PropType>(KnobProp prop, object value)
        {
            Property.Set<KnobProp>(this,
                                   this.bitmap.Lock,
                                   properties,
                                   prop,
                                   ValidateSetProperty<KnobProp, PropType>(prop, value));
        }

        private PropType ValidateSetProperty<Prop, PropType>(KnobProp prop, object value)
        {
            if (prop.Equals(KnobProp.CrossoverThreshold))
            {
                double newValue = (double)value;

                value = UtilMath.ValidateTreshold(newValue,
                                                  Minimum,
                                                  Maximum);
            }
            else if (prop.Equals(KnobProp.LargeChange))
            {
                double newLargeChange = (double)value;

                if (newLargeChange > Maximum - Minimum)
                {
                    value = Maximum - Minimum;
                }
            }
            else if (prop.Equals(KnobProp.Maximum))
            {
                double newMaximum = (double)value;

                if (newMaximum < Minimum)
                {
                    value = Minimum;
                }
            }
            else if (prop.Equals(KnobProp.Minimum))
            {
                double newMinimum = (double)value;

                if (newMinimum > Maximum)
                {
                    value = Maximum;
                }
            }
            else if (prop.Equals(KnobProp.SmallChange))
            {
                double newSmallChange = (double)value;

                if (newSmallChange > Maximum - Minimum)
                {
                    value = Maximum - Minimum;
                }
            }
            else if (prop.Equals(KnobProp.Value))
            {
                value = UtilMath.ClampValue((double)value, Minimum, Maximum);
            }
            
            return (PropType)value;
        }
        #endregion Private Methods
    }
}