﻿#region Using
using System;
#if (WINDOWS_UWP)
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Imaging;
#else
using System.Windows;
using System.Windows.Media.Imaging;
#endif
#endregion Using

namespace VSTControls.Net
{
    internal struct Bitmap
    {
        #region Internal Properties
        internal object Lock
        {
            get
            {
                if (@lock == null)
                {
                    @lock = new object();
                }

                return @lock;
            }
        }
        #endregion Internal Properties

        #region Internal Fields
        internal Rect rectangle;
        #if (WINDOWS_UWP)
        internal byte[] buffer;
        internal Size controlSize;
        internal WriteableBitmap image;
        #endif
        #endregion Internal Fields

        #region Private Fields
        private object @lock;
        #endregion Private Fields
                        
        #region Internal Methods
        internal void SetRectangle(Size controlSize)
        {
            Rect newRectangle = new Rect(0, 0, controlSize.Width, controlSize.Height);

            if (ValidateRectangle(newRectangle))
            {
                rectangle = newRectangle;
            }
        }
        #endregion Internal Methods

        #region Private Methods
        private bool ValidateRectangle(Rect rectangle)
        {
            return rectangle.X >= 0 &&
                   rectangle.X < double.PositiveInfinity &&
                   rectangle.Y >= 0 &&
                   rectangle.Y < double.PositiveInfinity &&
                   rectangle.Width >= 0 &&
                   rectangle.Width < double.PositiveInfinity &&
                   rectangle.Height >= 0 &&
                   rectangle.Height < double.PositiveInfinity;
        }
        #endregion Private Methods
    }
}