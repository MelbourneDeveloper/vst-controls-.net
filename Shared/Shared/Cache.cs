﻿#region Using
using System.Collections.Generic;
#if (WINDOWS_UWP)
using Windows.UI.Xaml.Media.Imaging;
#else
using System.Windows.Media.Imaging;
#endif
#endregion Using

namespace VSTControls.Net
{
    internal static class Cache
    {
        #region Private Constants
        private const int predictedBitmapCount = 32;
        #endregion Private Constants

        #region Private Static Fields
        private static readonly Dictionary<string, WriteableBitmap> Bitmaps = new Dictionary<string, WriteableBitmap>(predictedBitmapCount);
        #endregion Private Static Fields

        #region Internal Static Methods
        internal static void AddBitmap(string path, WriteableBitmap bitmap)
        {
            if (bitmap != null &&
                !string.IsNullOrWhiteSpace(path) &&
                !Bitmaps.ContainsKey(path))
            {
                Bitmaps.Add(path, bitmap);
            }
        }

        internal static WriteableBitmap GetBitmap(string path)
        {
            WriteableBitmap bitmap = null;

            if (!string.IsNullOrWhiteSpace(path))
            {
                Bitmaps.TryGetValue(path, out bitmap);
            }

            return bitmap;
        }
        #endregion Internal Static Methods
    }
}
