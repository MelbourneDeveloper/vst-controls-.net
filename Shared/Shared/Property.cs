﻿#region Using
using System;
using System.Collections.Generic;
#if (WINDOWS_UWP)
using Windows.UI.Xaml;
#else
using System.Windows;
#endif
#endregion Using

namespace VSTControls.Net
{
    #region Internal Enums
    internal enum KnobProp
    {
        CrossoverThreshold,
        EndAngle,
        ImageStretch,
        IntegerMaximum,
        IntegerMinimum,
        IntegerValue,
        IsKeyboardEnabled,
        KnobImage,
        LargeChange,
        Maximum,
        Minimum,
        SingleMaximum,
        SingleMinimum,
        SingleValue,
        SmallChange,
        StartAngle,
        Value,
    }

    internal enum OnOffSwitchProp
    {
        ImageStretch,
        IsKeyboardEnabled,
        IsMouseWheelInverted,
        IsOn,
        OffImage,
        OnImage
    }

    internal enum PictureProp
    {
        ImageStretch,
        PictureImage
    }

    internal enum SliderProp
    {
        BackgroundImage,
        ForegroundImage,
        ImageStretch,
        IntegerMaximum,
        IntegerMinimum,
        IntegerValue,
        IsKeyboardEnabled,
        LargeChange,
        Maximum,
        Minimum,
        Orientation,
        SingleMaximum,
        SingleMinimum,
        SingleValue,
        SmallChange,
        ThumbImage,
        TickFrequency,
        Value
    }
    #endregion Internal Enums

    internal static partial class Property
    {
        #region Internal Static Fields
        internal static readonly int KnobPropCount = Enum.GetValues(typeof(KnobProp)).Length;
        internal static readonly int OnOffSwitchPropCount = Enum.GetValues(typeof(OnOffSwitchProp)).Length;
        internal static readonly int PicturePropCount = Enum.GetValues(typeof(PictureProp)).Length;
        internal static readonly int SliderPropCount = Enum.GetValues(typeof(SliderProp)).Length;
        #endregion Internal Static Fields

        #region Private Static Fields
        private static readonly FrameworkPropertyMetadataOptions bindingType = FrameworkPropertyMetadataOptions.BindsTwoWayByDefault;
        #endregion Private Static Fields

        #region Internal Static Methods
        internal static PropType Get<Prop, PropType>(DependencyObject @this, object @lock, Dictionary<Prop, DependencyProperty> properties, Prop prop)
        {
            lock (@lock)
            {
                try
                {
                    return (PropType)@this.GetValue(properties[prop]);
                }
                catch (Exception exception)
                {
                    Message.Output(Message.Text[Error.Technical], exception.ToString());

                    return (PropType)Default.Values[prop];
                }
            }
        }

        internal static void Set<Prop>(DependencyObject @this, object @lock, Dictionary<Prop, DependencyProperty> properties, Prop prop, object value)
        {
            lock (@lock)
            {
                try
                {
                    @this.SetValue(properties[prop], value);
                }
                catch (Exception exception)
                {
                    Message.Output(Message.Text[Error.Technical], exception.ToString());
                }
            }
        }
        
        internal static void Register<Control, Prop>(Dictionary<Prop, DependencyProperty> properties, PropertyChangedCallback messagePump)
        {
            foreach (Prop prop in Enum.GetValues(typeof(Prop)))
            {
                properties.Add(prop, Register(Enum.GetName(typeof(Prop), prop),
                                              typeof(Control),
                                              Default.Values[prop],
                                              messagePump));
            }
        }
        #endregion Internal Static Methods

        #region Private Static Methods
        private static DependencyProperty Register(string name, 
                                                   Type controlType, 
                                                   object defaultValue, 
                                                   PropertyChangedCallback messagePump)
        {
            return DependencyProperty.Register(name,
                                               defaultValue.GetType(),
                                               controlType,
                                               new FrameworkPropertyMetadata(defaultValue,
                                                                             bindingType,
                                                                             messagePump));
        }

        private static DependencyProperty Override(string name,
                                                   Type controlType,
                                                   object defaultValue,
                                                   PropertyChangedCallback messagePump)
        {
            return DependencyProperty.Register(name,
                                               defaultValue.GetType(),
                                               controlType,
                                               new FrameworkPropertyMetadata(defaultValue,
                                                                             bindingType,
                                                                             messagePump));
        }
        #endregion Private Static Methods
    }
}