﻿#region Using
using System.Collections.Generic;
#if (WINDOWS_UWP)
using Windows.UI.Xaml.Controls;
#else
using System.Windows.Controls;
#endif
#endregion Using

namespace VSTControls.Net
{
    internal static class Default
    {
        #region Internal Static Fields
        internal static readonly Dictionary<object, object> Values = new Dictionary<object, object>(Property.KnobPropCount +
                                                                                                    Property.OnOffSwitchPropCount +
                                                                                                    Property.PicturePropCount +
                                                                                                    Property.SliderPropCount)
        {
            { KnobProp.CrossoverThreshold, 0.49d },
            { KnobProp.EndAngle, 0d },
            { KnobProp.ImageStretch, false },
            { KnobProp.IntegerMaximum, 1 },
            { KnobProp.IntegerMinimum, 0 },
            { KnobProp.IntegerValue, 0 },
            { KnobProp.IsKeyboardEnabled, true },
            { KnobProp.KnobImage, string.Empty },
            { KnobProp.LargeChange, 0.2d },
            { KnobProp.Maximum, 1d },
            { KnobProp.Minimum, 0d },
            { KnobProp.SingleMaximum, 1f },
            { KnobProp.SingleMinimum, 0f },
            { KnobProp.SingleValue, 0f },
            { KnobProp.SmallChange, 0.05d },
            { KnobProp.StartAngle, 0d },
            { KnobProp.Value, 0d },

            { OnOffSwitchProp.ImageStretch, false },
            { OnOffSwitchProp.IsKeyboardEnabled, true },
            { OnOffSwitchProp.IsMouseWheelInverted, false },
            { OnOffSwitchProp.IsOn, false },
            { OnOffSwitchProp.OffImage, string.Empty },
            { OnOffSwitchProp.OnImage, string.Empty },

            { PictureProp.ImageStretch, false },
            { PictureProp.PictureImage, string.Empty },
            
            { SliderProp.BackgroundImage, string.Empty },
            { SliderProp.ForegroundImage, string.Empty },
            { SliderProp.ImageStretch, false },
            { SliderProp.IntegerMaximum, 1 },
            { SliderProp.IntegerMinimum, 0 },
            { SliderProp.IntegerValue, 0 },
            { SliderProp.IsKeyboardEnabled, true },
            { SliderProp.LargeChange, 0.2d },
            { SliderProp.Maximum, 1d },
            { SliderProp.Minimum, 0d },
            { SliderProp.Orientation, Orientation.Vertical },
            { SliderProp.SingleMaximum, 1f },
            { SliderProp.SingleMinimum, 0f },
            { SliderProp.SingleValue, 0f },
            { SliderProp.SmallChange, 0.05d },
            { SliderProp.ThumbImage, string.Empty },
            { SliderProp.TickFrequency, 0d },
            { SliderProp.Value, 0d }
        };
        #endregion Internal Static Fields
    }
}
