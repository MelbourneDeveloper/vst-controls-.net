﻿#region Using
using System;
using System.Collections.Generic;
using System.Diagnostics;
#endregion Using

namespace VSTControls.Net
{
    #region Public Delegate
    #if !(WINDOWS_UWP)
    [CLSCompliant(true)]
    #endif
    /// <summary>
    /// Delegate for message output
    /// </summary>
    public delegate void MessageCallbackDelegate(params object[] args);
    #endregion Public Delegate

    #region Public Enums
    #if !(WINDOWS_UWP)
    [CLSCompliant(true)]
    #endif
    /// <summary>
    /// Error codes
    /// </summary>
    public enum Error
    {
        InvalidImageBounds,
        LoadImage,
        Technical
    }

    #if !(WINDOWS_UWP)
    [CLSCompliant(true)]
    #endif
    /// <summary>
    /// Warning codes
    /// </summary>
    public enum Warning
    {
        WrongDpi
    }
    #endregion Public Enums

    #if !(WINDOWS_UWP)
    [CLSCompliant(true)]
    #endif
    /// <summary>
    /// Class for VSTControls.Net Message output
    /// </summary>
    public static class Message
    {
        #region Public Static Fields
        #if !(WINDOWS_UWP)
        public 
        #else
        internal
        #endif
        /// <summary>
        /// Mapping of error and warning codes to text messages
        /// </summary>
        static readonly Dictionary<object, string> Text = new Dictionary<object, string>(ErrorCount + WarningCount)
        {
            { Error.InvalidImageBounds, "Error, invalid image bounds: " },
            { Error.LoadImage, "Error, failed to load image: " },
            { Error.Technical, "Error, a vexing exception was raised and ignored: " },
            { Warning.WrongDpi, "Warning, convert images to 96 DPI for the best visual quality: " }
        };

        #if !(WINDOWS_UWP)
        public 
        #else
        internal
        #endif
        /// <summary>
        /// Mapping of text messages to error codes
        /// </summary>
        static readonly Dictionary<string, Error> TextToErrorCode = new Dictionary<string, Error>(ErrorCount);
        
        #if !(WINDOWS_UWP)
        public 
        #else
        internal
        #endif
        /// <summary>
        /// Mapping of text messages to warning codes
        /// </summary>
        static readonly Dictionary<string, Warning> TextToWarningCode = new Dictionary<string, Warning>(WarningCount);
        #endregion Public Static Fields

        #region Private Static Fields
        private static readonly int ErrorCount = Enum.GetValues(typeof(Error)).Length;
        private static readonly int WarningCount = Enum.GetValues(typeof(Warning)).Length;
        #endregion Private Static Fields

        #region Static Constructor
        static Message()
        {
            // Build errors and warnings mapping hash table
            foreach (Object key in Text.Keys)
            {
                if (key.GetType().Equals(typeof(Error)))
                {
                    TextToErrorCode.Add(Text[key], (Error)key);
                }
                else if (key.GetType().Equals(typeof(Warning)))
                {
                    TextToWarningCode.Add(Text[key], (Warning)key);
                }
            }
        }
        #endregion Static Constructor

        #region Public Static Methods
        /// <summary>
        /// Set delegate for message output
        /// </summary>
        /// <param name="messageCallbackDelegate">Delegate for message output</param>
        public static void SetMessageCallback(MessageCallbackDelegate messageCallbackDelegate)
        {
            Output = messageCallbackDelegate;
        }
        #endregion Public Static Methods

        #region Internal Static Methods
        internal static MessageCallbackDelegate Output = delegate (object[] args)
        {
            Debug.WriteLine(string.Join(Environment.NewLine, args));
        };
        #endregion Internal Static Methods
    }
}