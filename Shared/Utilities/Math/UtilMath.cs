﻿#region Using
using System;
#if (WINDOWS_UWP)
using Windows.Foundation;
#else
using System.Windows;
#endif
#endregion Using

namespace VSTControls.Net
{
    internal static class UtilMath
    {
        #region Private Constants
        // Various incantations of dubious practical value
        public const double Epsilon = 0.01d;
        private const double FullCircle = 360d;
        private const double Half = 0.5d;
        private const double HalfCircle = 180d;
        private const double StartCircle = 0d;
        #endregion Private Constants

        #region Internal Static Methods
        internal static double ClampValue(double value, double min, double max)
        {
            return ClampValue(value,
                              value,
                              min,
                              max,
                              0d);
        }

        internal static double ClampValue(double oldValue, double newValue, double min, double max, double treshold)
        {
            if (treshold > 0 && 
                oldValue < newValue &&
                oldValue >= min && 
                oldValue <= min + treshold &&
                newValue >= max - treshold &&
                newValue <= max)
            {
                // If value is crossing over min/max towards min without reaching treshold return min.
                return min;
            }
            else if (treshold > 0 &&
                     oldValue > newValue &&
                     oldValue >= max - treshold && 
                     oldValue <= max &&
                     newValue >= min && 
                     newValue <= min + treshold)
            {
                // If value is crossing over min/max towards max without reaching treshold return max.
                return max;
            }
            else if (newValue < min)
            {
                return min;
            }
            else if (newValue > max)
            {
                return max;
            }
            else
            {
                return newValue;
            }
        }

        internal static double ConvertAngleToValue(Rect rectangle, Point point, double startAngle, double endAngle, double minimum, double maximum)
        {
            // Convert point angle degree value relative to center of rectangle
            // to the equivalent linearly extrapolated value in min/max range.
            return ConvertRange(ClampAngle(ComputeAngle(rectangle, point),
                                           startAngle,
                                           endAngle),
                                StartCircle,
                                FullCircle - Math.Abs(startAngle - endAngle),
                                minimum,
                                maximum);
        }

        internal static double ConvertRange(double oldValue, double oldMin, double oldMax, double newMin, double newMax)
        {
            double newValue;
            double oldRange = (oldMax - oldMin);
            double newRange = (newMax - newMin);

            // Prevent division by 0 when old range is empty
            if (oldRange.Equals(0d))
            {
                oldRange = 1d;
            }

            // Convert value in old range to the equivalent linearly extrapolated value in new range.
            newValue = (((oldValue - oldMin) * newRange) / oldRange) + newMin;

            return newValue;
        }

        internal static double ValidateTreshold(double treshold, double minimum, double maximum)
        {
            // Get value of half range minus epsilon
            double middleRangle = (maximum - minimum) * Half - Epsilon;

            // Treshold value should not exceed half range
            return treshold > middleRangle ? middleRangle : treshold;
        }

        private static double ClampAngle(double angle, double startAngle, double endAngle)
        {
            // If value is outside StartAngle/EndAngle range
            if (angle < startAngle && angle > endAngle)
            {
                // Find middle of range between StartAngle/EndAngle range
                double deadRangeCenter = endAngle + (startAngle - endAngle) * Half;

                // Set angle to start or end angle depending on angle distance from center of dead range.
                angle = angle > deadRangeCenter ? startAngle : endAngle;
            }

            // Adjust angle relative to start angle offset
            if (angle >= startAngle)
            {
                angle -= startAngle;
            }
            else
            {
                angle += FullCircle - startAngle;
            }

            return angle;
        }
        #endregion Internal Static Methods

        #region Private Static Methods
        private static double ComputeAngle(Rect rect, Point point)
        {
            // Dear God Atan II and PI Deity, accept this strong typed offering and gimme the angle OK? will ya? plz plz plz
            return ((Math.Atan2(point.X - rect.Width * Half,
                                -(point.Y - rect.Height * Half)) * HalfCircle / Math.PI) + FullCircle) % FullCircle;
        }
        #endregion Private Static Methods
    }
}