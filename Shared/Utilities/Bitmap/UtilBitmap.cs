﻿#region Using
using System;
#endregion Using

namespace VSTControls.Net
{
    internal static partial class UtilBitmap
    {
        #region Internal Static methods
        internal static int ComputeSpritePositionInSpriteSheet(int imageIndex, int spriteSize, int spriteSheetHeight)
        {
            // Quantize the sprite Y position in the sprite sheet to a multiple of the sprite size.
            int y = spriteSize * imageIndex / spriteSize * spriteSize;

            // Bounds check
            if (y < 0 ||
                y > spriteSheetHeight - spriteSize)
            {
                throw new Exception(Message.Text[Error.InvalidImageBounds]);
            }

            return y;
        }
        #endregion Internal Static methods
    }
}