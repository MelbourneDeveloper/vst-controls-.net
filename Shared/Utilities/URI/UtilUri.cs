﻿#region Using
using System;
using System.Globalization;
using System.IO;
#endregion Using

namespace VSTControls.Net
{
    internal static partial class UtilUri
    {
        #region Internal Constants
        internal const string SpriteSuffix = "000";
        #endregion Internal Constants

        #region Internal Static Methods
        internal static string BuildSpriteUri(int spriteIndex, string directory, string filename)
        {
            // Replace image suffix in filename by image suffix based on sprite index.
            // Ex: 'img_000.png' becomes 'img_003.png' for a sprite index value of 3.
            return string.Concat(directory,
                                 filename.Replace(SpriteSuffix,
                                                  Convert.ToString(spriteIndex, 
                                                                   CultureInfo.InvariantCulture).PadLeft(SpriteSuffix.Length,
                                                                                                         SpriteSuffix[0])));
        }

        internal static string BuildUri(string uri)
        {
            if (string.IsNullOrWhiteSpace(uri))
                return null;

            try
            {
                // Best way to test this is to catch the exception.
                string path = Path.Combine(basePath, uri);

                return File.Exists(path) ? path : uri;
            }
            catch
            {
                return string.Empty;
            }
        }

        internal static void ExtractUriComponents(string uri, out string directory, out string filename)
        {
            filename = Path.GetFileName(uri);
            directory = uri.Replace(filename, string.Empty);
        }
        #endregion Internal Static Methods
    }
}
